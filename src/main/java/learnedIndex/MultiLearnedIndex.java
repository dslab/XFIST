package learnedIndex;

import gnu.trove.list.TDoubleList;
import gnu.trove.list.TIntList;
import gnu.trove.list.array.TDoubleArrayList;

import java.io.Serializable;
import java.util.BitSet;
import java.util.List;

public class MultiLearnedIndex implements Serializable {
    //default serialVersion id
    private static final long serialVersionUID = 1L;


    protected SortedArray array;
    protected List<LinearRegression>[] lrSetArray;
    protected TDoubleArrayList[] lrHeadSet;
    protected TIntList lastPositionInPartition;
    protected BitSet isMin;


    public TDoubleArrayList[] getLrHeadSet() {
        return lrHeadSet;
    }

    public void setLrHeadSet(TDoubleArrayList[] lrHeadSet) {
        this.lrHeadSet = lrHeadSet;
    }

    public void setLastPositionInPartition(TIntList lastPositionInPartition) {
        this.lastPositionInPartition = lastPositionInPartition;
    }

    public List<LinearRegression>[] getLrSetArray() {
        return lrSetArray;
    }

    public void setLrSetArray(List<LinearRegression>[] lrSetArray) {
        this.lrSetArray = lrSetArray;
    }

    public SortedArray getArray() {
        return array;
    }

    public void setArray(SortedArray array) {
        this.array = array;
    }


    public int getFirstPositionAtPartition(int iPartition){
        if(iPartition == 0)
            return 0;
        return lastPositionInPartition.get(iPartition-1)+1;
    }

    public int getLastPositionAtPartition(int iPartition){
        return lastPositionInPartition.get(iPartition);
    }

    public boolean isMinAt(int pos){
        return isMin.get(pos);
    }


    public TIntList getArrayPtrSubList(int start, int end){
        return array.getPtrSubList(start,end);
    }
    public TDoubleList getArrayKeySubList(int start, int end){

        return array.getKeySubList(start,end);
    }
    public double getKeyAt(int pos){
        return array.getKeyAt(pos);
    }
}
