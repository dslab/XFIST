package learnedIndex;

import gnu.trove.list.TDoubleList;
import gnu.trove.list.TIntList;
import gnu.trove.list.array.TDoubleArrayList;
import gnu.trove.list.array.TFloatArrayList;
import gnu.trove.list.array.TIntArrayList;

import java.io.Serializable;
import java.util.Collections;

public class SortedArray implements Serializable {
    //default serialVersion id
    private static final long serialVersionUID = 1L;

    private TDoubleArrayList keys;
    private TIntArrayList ptr;

    public int getLength(){
        return keys.size();
    }

    public int getPointerAt(int pos){
        return ptr.get(pos);
    }
    public double getKeyAt(int pos){
        return keys.get(pos);
    }


    public void setKeys(TDoubleArrayList keys){
        this.keys = keys;
    }
    public void setPtr(TIntArrayList ptr){
        this.ptr = ptr;
    }

    public TIntList getPtrSubList(int start, int end){
        return ptr.subList(start,end);
    }
    public TDoubleList getKeySubList(int start, int end){
        return keys.subList(start,end);
    }
}
