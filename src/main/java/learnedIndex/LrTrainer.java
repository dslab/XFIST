package learnedIndex;

import gnu.trove.list.TDoubleList;
import gnu.trove.list.array.TDoubleArrayList;

public class LrTrainer {
    LinearRegression lr;


    public void train(TDoubleList x, TDoubleList y, int n, double error){
        if(n==1 || x.get(0) == x.get(n-1)) {
            lr = new LinearRegression(0, (y.get(n-1)+y.get(0))/2, 0,
                     x.get(0),x.get(n-1));
            setLrError(x,y,n);
            return;
        }
        else if(n<=error){
            generateLr2point(x.get(0),x.get(n-1),y.get(0),y.get(n-1));
            setLrError(x,y,n);
            return;
        }

        TDoubleArrayList inv = inv2D2x2(naiveMultSelfCustX1(x,n));
        if(inv == null){
            generateLr2point(x.get(0),x.get(n-1),y.get(0),y.get(n-1));
        }
        else {
            TDoubleArrayList xty = naiveMultXTY(x, y, n);
            TDoubleArrayList b = naiveMultInvXTY(inv, xty);
            lr = new LinearRegression(b.get(1),  b.get(0), 0,
                     x.get(0),x.get(n-1));
        }
        setLrError(x,y,n);
    }

    private void generateLr2point(double x1, double x2, double y1, double y2){
        double coef = (y2-y1)/(x2-x1);
        double intercept = y1 - (coef*x1);
        lr = new LinearRegression(coef, intercept, 0, x1, x2);

    }

    private void setLrError(TDoubleList x, TDoubleList y, int n){
        lr.error = computeMaxError(lr.coef, lr.intercept, x,y,n);
    }

    public LinearRegression getLr(){
        return lr;
    }

    public double computeMaxError(double coefficient, double intercept, TDoubleList x, TDoubleList y, int n){

        double maxError = 0;

        for(int i=0;i<n;i++){

            double att = x.get(i);
            double yAtt = y.get(i);

            double prediction = (coefficient * att) + intercept;

            double trError = prediction - yAtt;
            if(maxError<trError)
                maxError = trError;
            else if(maxError<-1*trError)
                maxError = -1*trError;
        }

        return maxError ;
    }


    private void dosomething(){}


    public TDoubleArrayList naiveMultSelfCustX1(TDoubleList x, int n){
        TDoubleArrayList res = new TDoubleArrayList();
        res.add(n);
        res.add(x.sum());

        double sum2 = 0;
        for(int i=0;i<n;i++){
            sum2 = sum2 + (x.get(i)*x.get(i));
        }
        res.add(sum2);

        return res;

    }

    public TDoubleArrayList naiveMultXTY(TDoubleList x, TDoubleList y, int n){
        TDoubleArrayList res = new TDoubleArrayList();
        res.add(y.sum());

        double sumXY = 0;
        for(int i=0;i<n;i++){
            sumXY = sumXY + (x.get(i) * y.get(i));
        }
        res.add(sumXY);
        return res;
    }

    public TDoubleArrayList inv2D2x2(TDoubleArrayList a){
        double divisor = (a.get(0) * a.get(2)) - (a.get(1) * a.get(1));
        if(divisor == 0)
            return null;
        TDoubleArrayList inv = new TDoubleArrayList();

        inv.add(a.get(2)/divisor);
        inv.add(-a.get(1)/divisor);
        inv.add(a.get(0)/divisor);
        return inv;
    }

    public TDoubleArrayList naiveMultInvXTY(TDoubleArrayList inv, TDoubleArrayList xty){
        TDoubleArrayList params = new TDoubleArrayList();
        params.add((inv.get(0) * xty.get(0))+(inv.get(1) * xty.get(1)));
        params.add((inv.get(1) * xty.get(0))+(inv.get(2) * xty.get(1)));

        return params;
    }

}
