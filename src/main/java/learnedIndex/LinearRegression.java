package learnedIndex;

import java.io.Serializable;

public class LinearRegression implements Serializable {

    public double intercept;
    public double coef;
    public double error;
    public double head;
    public double tail;

    public LinearRegression(double coefficient, double intercept, double error,
                            double head, double tail) {
        this.coef = coefficient;
        this.intercept = intercept;
        this.error = error;
        this.head = head;
        this.tail = tail;
    }
}
