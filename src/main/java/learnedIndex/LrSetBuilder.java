package learnedIndex;

import gnu.trove.list.TDoubleList;
import gnu.trove.list.TFloatList;
import gnu.trove.list.array.TDoubleArrayList;

import java.util.ArrayList;
import java.util.List;

public class LrSetBuilder {
    private TDoubleArrayList x;
    private TDoubleArrayList y;


    private ArrayList<LinearRegression> lrSet;
    private TDoubleArrayList lrSetFirstValues;

    public void createTrainDataset(double[] keys){

        x = new TDoubleArrayList(keys);
        y = new TDoubleArrayList();

        for(int i=0;i<keys.length;i++){
            y.add(i);
        }
    }


    public void createTrainDataset(TDoubleList keys, TDoubleList target){

        x = new TDoubleArrayList(keys);
        y = new TDoubleArrayList(target);

    }


    public void createTrainDataset(TFloatList keys, TFloatList target){

        x = new TDoubleArrayList();
        y = new TDoubleArrayList();
        for(int i=0;i<keys.size();i++) {
            x.add(keys.get(i));
            y.add(target.get(i));
        }

    }

    public void createTrainDataset(float[] keys){

        x = new TDoubleArrayList();
        for(int i=0;i<keys.length;i++)
            x.add(keys[i]);
        y = new TDoubleArrayList();

        for(int i=0;i<keys.length;i++){
            y.add(i);
        }
    }


    private int findSub(int begin, int size){
        if(x.get(begin) == x.get(begin+size-1))
            return begin+size-1;
        int sub = size/2;
        while(x.get(begin)==x.get(begin+sub))
            sub++;

        while(x.get(begin+sub-1)==x.get(begin+size-1))
            sub--;

        while(x.get(begin+sub-1)==x.get(begin+sub))
            sub++;
        return sub;
    }

    private boolean checkTrainingOk(LinearRegression lr, int begin, int end, double error){
        if(x.get(begin) == x.get(end))
            return true;
        return lr.error<= error;
    }

    public void buildLrSetv2(double error){

        int batchSize = (int) error;
        int firstIndex = 0;
        int lastIndex, prevLastIndex = firstIndex;
        LrTrainer lptt = new LrTrainer();

        LinearRegression lr, lrBeforeExp = null;
        lastIndex = firstIndex + batchSize;

        lrSet  = new ArrayList<>();
        lrSetFirstValues = new TDoubleArrayList();

        while(firstIndex<x.size()){
            if(lastIndex > x.size()) {
                lastIndex = x.size();
            }

            if(x.get(firstIndex) == x.get(lastIndex-1)) {
                while(x.get(firstIndex) == x.get(lastIndex-1)) {
                    lastIndex++;
                    if (lastIndex > x.size()) {
                        break;
                    }
                }
                lastIndex--;
            }
            while(lastIndex> firstIndex && lastIndex < x.size()
                    && x.get(lastIndex-1) == x.get(lastIndex))
                lastIndex--;

            TDoubleList xSub = x.subList(firstIndex,lastIndex);
            TDoubleList ySub = y.subList(firstIndex,lastIndex);
            lptt.train(xSub,ySub,lastIndex-firstIndex,error);
            lr = lptt.getLr();
            if((lr.error <= error || lr.head == lr.tail) && prevLastIndex != lastIndex) {
                prevLastIndex = lastIndex;
                lastIndex = lastIndex + batchSize;
                lrBeforeExp = lr;
            }
            else{
                if(prevLastIndex == lastIndex)
                    lastIndex = lastIndex + batchSize;
                firstIndex = prevLastIndex;
                lrSet.add(lrBeforeExp);
                lrSetFirstValues.add(lrBeforeExp.head);
            }

        }
        lrSet.add(lrBeforeExp);
        lrSetFirstValues.add(lrBeforeExp.head);

    }

    public void buildLrSet(double error){
        int start = 0;
        int size = x.size();
        if(size == 0){

            lrSet  = null;
            lrSetFirstValues = null;
            return;
        }
        int fullSize = x.size();
        lrSet  = new ArrayList<>();
        lrSetFirstValues = new TDoubleArrayList();
        LrTrainer lptt = new LrTrainer();

        lptt.train(x,y,fullSize,error);
        LinearRegression lr = lptt.getLr();

        boolean toTrain = !checkTrainingOk(lr, start, start+size-1, error);

        if(!toTrain){
            lrSet.add(lr);
            lrSetFirstValues.add(x.get(0));
            return;
        }
        while(toTrain){
            int sub = findSub(start,size);

            TDoubleList xSub = x.subList(start,start+sub), ySub=y.subList(start,start+sub);
            lptt.train(xSub,ySub,sub,error);
            lr = lptt.getLr();

            if(checkTrainingOk(lr,start,start+sub-1,error)){
                lrSet.add(lr);
                lrSetFirstValues.add(xSub.get(0));
                start = start+sub;
                size = fullSize - start;

                xSub=x.subList(start,start+size);
                ySub=y.subList(start,start+size);
                lptt.train(xSub,ySub,size,error);
                lr = lptt.getLr();
                toTrain = !checkTrainingOk(lr, start, start+size-1, error);

                if(!toTrain){
                    lrSet.add(lr);
                    lrSetFirstValues.add(xSub.get(0));
                }
            }
            else{
                size = sub;
            }


/*
            int sizeToCopy = size - startIndex;

            boolean toTrain = true;
            while(toTrain){
                last = startIndex +sizeToCopy;
                // if the subarray has the same value of
                // [...9, 10, 11, 11, 11, 11][11, 11, 11, 12 ...]
                //we should make the subarray as
                // [...9, 10, 11, 11, 11, 11, 11, 11, 11][12 ...]
                if(last <size-1){
                    if(x.get(last - 1) == x.get(last))
                        last = findUpperBoundSameValue(x,last,size,x.get(last)) + 1;
                }

                //System.out.println("X " +startIndex+ " " + last);
                xSub = x.subList(startIndex, last);
                ySub = y.subList(startIndex, last);
                lptt.train(xSub,ySub,last-startIndex,error);
                lr = lptt.getLr();

                //if the trained lr to the subarray has worse
                // error value than threshold...
                // [...9, 10, 11, 11, 11, 11, 11, 11, 11][12 ...]
                // we retrain the lr to this subarray
                // [...9, 10][11, 11, 11, 11, 11, 11, 11, 12 ...]

                    //                if(startIndex == 249891 ){
                    //                        doSomething();
                    //                        lptt.train(xSub,ySub,last-startIndex,error);
                    //                        lr = lptt.getLr();
                    //                }
                if((lr.errorPos-lr.errorNeg) > error &&
                    x.get(last-2) == x.get(last-1) ){
                    if(x.get(last-1) == x.get(startIndex))
                        toTrain = false;
                    else {
                        last = startIndex + sizeToCopy;
                        double sameValue = x.get(last - 2);

                        last = findLowerBoundSameValue(x,startIndex,last-1,sameValue);
                        //System.out.println("Y "+startIndex+ " " + last);
                        xSub = x.subList(startIndex, last);
                        ySub = y.subList(startIndex, last);
                        lptt.train(xSub, ySub, last - startIndex, error);
                        lr = lptt.getLr();


                    }
                }

                sizeToCopy = last-startIndex;
                errorNeg = lr.errorNeg;
                errorPos = lr.errorPos;

                if(toTrain) {
                    if((errorPos - errorNeg)<=error)
                        toTrain = false;
                    else
                        sizeToCopy = sizeToCopy/2;
                }
            }

            lrSet.add(lr);
            //System.out.println("Z "+startIndex+ " " + last);
            lrSetFirstValues.add(xSub.get(0));
            if(last < size){
                errorNeg = - error - 1;
                errorPos = error + 1;
                startIndex = last;
            }
            */
        }
    }

    private int findUpperBoundSameValue(TDoubleArrayList x, int lower, int upper, double sameValue){
        int mid;
        while(upper - lower > 1){
            mid = (upper + lower)/2;
            if(x.get(mid)==sameValue)
                lower = mid;
            else
                upper = mid;
        }
        return lower;
    }

    private int findLowerBoundSameValue(TDoubleArrayList x, int lower, int upper, double sameValue){
        int mid;
        while(upper - lower > 1){
            mid = (upper + lower)/2;
            if(x.get(mid)==sameValue)
                upper = mid;
            else
                lower = mid;
        }
        return upper;
    }

    private void doSomething(){}

    public List<LinearRegression> getLrSet(){
        return lrSet;
    }

    public TDoubleArrayList getLrSetFirstValues(){
        return lrSetFirstValues;
    }


}
