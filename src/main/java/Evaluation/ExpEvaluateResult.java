package Evaluation;

import Exp.ExpConfig;
import Utils.Distance;
import Utils.TrajUtil;
import Utils.Util;
import gnu.trove.list.TIntList;
import gnu.trove.list.array.TDoubleArrayList;
import gnu.trove.list.array.TIntArrayList;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ExpEvaluateResult {

    private static List<TIntArrayList> ids;
    private static List<TIntArrayList> idsPruned;
    private static List<TDoubleArrayList> distances;
    private static int fullSize;

    public static void init(){
        switch (ExpConfig.datasetFilename){
            case "chengdu":
                fullSize = ExpConfig.chengduSize;
                break;
            case "atc-x-normal":
                fullSize = ExpConfig.atcSize;
                break;
            case "porto":
                fullSize = ExpConfig.portoSize;
                break;
        }

    }

    public static double[] computeMetricsWithTestI(TIntList retrieved, int i){
        return computeMetrics(retrieved,ids.get(i));
    }
    public static double[] computeMetrics(TIntList retrieved, TIntArrayList groundTruth){
        double recall, precision, accuracy;
        int truePos=0, trueNeg, falsePos =0, falseNeg=0;
        retrieved.sort();
        groundTruth.sort();

        int i=0,j=0;
        while(true){
            if(i==retrieved.size() || j ==groundTruth.size())
                break;
            int r_i = retrieved.get(i);
            int r_j = retrieved.get(j);
            if(r_i < r_j) {
                falsePos++;
                i++;
            }
            else if(r_i > r_j) {
                falseNeg++;
                j++;
            }
            else{
                truePos++;
                i++;j++;
            }
        }
        falseNeg += (groundTruth.size()-j);
        falsePos += (retrieved.size()-i);
        trueNeg = fullSize - (truePos + falseNeg + falsePos);

        accuracy = ((1.0 * truePos) + trueNeg) / fullSize;
        precision = (1.0 * truePos) / (truePos +falsePos);
        recall =  (1.0 * truePos) / (truePos +falseNeg);

        System.out.println("TP:" +truePos+ " TN:"+ trueNeg +" FP:"+ falsePos+" FN:"+falseNeg);
        System.out.println("ACC " +String.format("%.3f",accuracy) +
                " PRECISION " +String.format("%.3f",precision) +
                " RECALL " +String.format("%.3f",recall));

        return new double[]{accuracy,precision,recall};
    }

    public static void getFilteredGroundTruth(String filename){
        ids = new ArrayList<>();
        distances = new ArrayList<>();
        try  {
            BufferedReader br = new BufferedReader(new FileReader(filename));
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(";");
                TIntArrayList gtIds = new TIntArrayList();
                TDoubleArrayList gtDistance = new TDoubleArrayList();
                for(String s:values){
                    double[] idAndDistance = Util.parseIdAndDistance(s);
                    if(idAndDistance[1] <= ExpConfig.tau) {
                        gtIds.add((int) idAndDistance[0]);
                        gtDistance.add(idAndDistance[1]);
                    }
                }
                ids.add(gtIds);
                distances.add(gtDistance);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void getPruned(String filename){
        idsPruned = new ArrayList<>();
        try  {
            BufferedReader br = new BufferedReader(new FileReader(filename));
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(",");
                TIntArrayList gtIds = new TIntArrayList();
                for(String s:values){
                    gtIds.add(Integer.parseInt(s.trim()));
                }
                idsPruned.add(gtIds);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void evaluate(){
        double avgAcc = 0, avgPrecision = 0, avgRecall=0;
        for(int i=0;i<idsPruned.size();i++) {
            double[] metrics = computeMetrics(idsPruned.get(i), ids.get(i));
            avgAcc =  avgAcc + metrics[0];
            avgPrecision = avgPrecision+  metrics[1];
            avgRecall = avgRecall+ metrics[2];

        }
        System.out.println("AVG_ACC " +String.format("%.3f",avgAcc/idsPruned.size()) +
                " AVG_PRECISION " +String.format("%.3f",avgPrecision/idsPruned.size()) +
                " AVG_RECALL " +String.format("%.3f",avgRecall/idsPruned.size()));

    }
}
