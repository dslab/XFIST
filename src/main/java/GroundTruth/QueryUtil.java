package GroundTruth;

import Utils.Distance;
import Utils.TrajUtil;
import Utils.Util;
import gnu.trove.list.TIntList;
import gnu.trove.list.array.TDoubleArrayList;
import gnu.trove.list.array.TIntArrayList;
import gnu.trove.map.hash.TIntDoubleHashMap;

import java.util.BitSet;
import java.util.List;
import java.util.PriorityQueue;

public class QueryUtil {
    private static TIntList resultIds;
    private static TDoubleArrayList resultDistances;
    private static BitSet examined;

    private static PriorityQueue<Util.IntDblPair> prunedBuffer;

    private static void initResults(){
        resultIds = new TIntArrayList();
        resultDistances = new TDoubleArrayList();
    }

    private static void initKnn(){
        examined = new BitSet();
        prunedBuffer = new PriorityQueue<>();
    }

    public static void searchSim(List<double[]> traj, double tau){
        initResults();
        for (int i = 0; i < TrajUtil.trajectoryDataset.size(); i++) {
            List<double[]> iTraj = TrajUtil.trajectoryDataset.get(i);

            //simple Pruning
            if(Distance.currentDistance == Distance.LCSS || Distance.currentDistance == Distance.EDR){
                if(Math.abs(traj.size() - iTraj.size()) > tau)
                    continue;
            } else {
                double distFirst = Util.computeDistance(traj.get(0),iTraj.get(0));
                double distLast = Util.computeDistance(traj.get(traj.size()-1),iTraj.get(iTraj.size()-1));
                if (Distance.currentDistance == Distance.FRECHET &&
                        (distFirst > tau || distLast > tau ))
                    continue;
                if(Distance.currentDistance == Distance.DTW &&
                        ((distFirst + distLast) > tau))
                    continue;
            }
            if(examined.get(i))
                continue;
            examined.set(i);

            double distance = TrajUtil.computeDistance(traj, iTraj);
            if (distance <= tau) {
                resultIds.add(i);
                resultDistances.add(distance);
            }
            else
                prunedBuffer.add(new Util.IntDblPair(i,distance));
        }
    }

    public static TIntList getResultIds(){
        return resultIds;
    }

    public static TDoubleArrayList getResultDistances(){
        return resultDistances;
    }

    public static TIntDoubleHashMap searchKnn(List<double[]> trajQuery, int k, double tauInit) {
        initKnn();
        Search.QueryUtil.initKnn(tauInit);

        while(Search.QueryUtil.isContinueKnn(k)) {
            long startTime = System.currentTimeMillis();
            double tau = Search.QueryUtil.getKnnTau();

            searchSim(trajQuery,tau);
            while(prunedBuffer.size()>0) {
                Util.IntDblPair idDist = prunedBuffer.peek();
                System.out.println(idDist);
                if (idDist.dblVal <= tau){
                    resultIds.add(idDist.intVal);
                    resultDistances.add(idDist.dblVal);
                    prunedBuffer.remove();
                }
                else
                    break;
            }
            System.out.println("Growing pruning time: "+
                    (System.currentTimeMillis()-startTime)+" ms");
            System.out.println("Examined: "+
                    examined.cardinality());
            Search.QueryUtil.searchKnnStep(trajQuery,k, resultIds, resultDistances);
            //System.out.println(resultIds);
        }
        TIntDoubleHashMap knnResult = Search.QueryUtil.outputKnnResult();
        return knnResult;
    }
}
