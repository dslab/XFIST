package GroundTruth;

import Exp.ExpConfig;
import Utils.Distance;
import Utils.TrajUtil;
import Utils.Util;
import gnu.trove.list.TIntList;
import gnu.trove.list.array.TDoubleArrayList;
import gnu.trove.list.array.TIntArrayList;
import gnu.trove.map.hash.TIntDoubleHashMap;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

public class ExperimentGroundTruth {
    private static  List<List<double[]>> testSet;
    public static void prepareParams() {
        switch (ExpConfig.distanceType){
            case "FRECHET":
                Distance.setCurrentDistance(Distance.FRECHET);
                Distance.setParameters(-1,-1);
                break;
            case "DTW":
                Distance.setCurrentDistance(Distance.DTW);
                Distance.setParameters(-1,-1);
                break;

            case "EDR":
                Distance.setCurrentDistance(Distance.EDR);
                Distance.setEps(ExpConfig.eps);
                break;
            case "LCSS":
                Distance.setCurrentDistance(Distance.LCSS);
                Distance.setEps(ExpConfig.eps);
                Distance.setSpaceConstraint(ExpConfig.spaceConstraint);
                break;
        }
        TrajUtil.loadTrajDatasetFromFile(ExpConfig.expPathDefault+ExpConfig.testPath+ExpConfig.testFile);
        //deepCopy
        testSet = new ArrayList<>();
        for(int i=0;i<TrajUtil.trajectoryDataset.size();i++){
            List<double[]> testTraj = new ArrayList<>();
            for(int j=0;j<TrajUtil.trajectoryDataset.get(i).size();j++){
                double[] point = new double[TrajUtil.trajectoryDataset.get(i).get(j).length];
                for(int k=0;k<point.length;k++)
                    point[k] = TrajUtil.trajectoryDataset.get(i).get(j)[k];
                testTraj.add(point);
            }
            testSet.add(testTraj);
        }
        TrajUtil.loadTrajDatasetFromFile(ExpConfig.datasetPathDefault+ExpConfig.datasetFilename+ExpConfig.datasetFileExt);

    }

    public static void runSearchSim(){
        System.out.println("Running query with groundTruth: "+ExpConfig.testFile);
        String fileAdd = "";


        fileAdd = String.format("%.3f",Distance.eps) + " ";
        if(Distance.currentDistance==Distance.LCSS)
            fileAdd = fileAdd+ Distance.spaceConstraint+" ";

        String tauStr = String.valueOf(ExpConfig.tau);
        File groundTruth = new File("groundTruth "
                + ExpConfig.datasetFilename+" " + Distance.getDistanceStr() +
                " "+ fileAdd + tauStr + ".csv");
        try {
            FileWriter fileWriter = new FileWriter(groundTruth);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

            for (int q = 0; q < testSet.size(); q++) {
                List<double[]> traj = testSet.get(q);
                GroundTruth.QueryUtil.searchSim(traj,ExpConfig.tau);
                TIntList resultIds = GroundTruth.QueryUtil.getResultIds();
                TDoubleArrayList resultDistances = GroundTruth.QueryUtil.getResultDistances();
                System.out.println(tauStr + " " + q + "/" + testSet.size());
                for (int i = 0; i < resultIds.size(); i++) {
                    bufferedWriter.write("("+resultIds.get(i) + ",");
                    bufferedWriter.write(resultDistances.get(i) + ");");
                }
                bufferedWriter.newLine();
                bufferedWriter.flush();

            }
            bufferedWriter.close();
        }catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static void runSearchKnn(){
        System.out.println("Running KNN query with test: "+ExpConfig.testFile +"(k="+ExpConfig.kKnn+")");
        String fileAdd = "";


        fileAdd = String.format("%.3f",Distance.eps) + " ";
        if(Distance.currentDistance==Distance.LCSS)
            fileAdd = fileAdd+ Distance.spaceConstraint+" ";

        String kStr = String.valueOf(ExpConfig.kKnn);
        File groundTruth = new File("groundTruthKnn "
                + ExpConfig.datasetFilename+" " + Distance.getDistanceStr() +
                " "+ fileAdd + "k"+kStr + ".csv");
        try {

            FileWriter fileWriter = new FileWriter(groundTruth);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

            for (int q = 0; q < testSet.size(); q++) {
                List<double[]> traj = testSet.get(q);
                TIntDoubleHashMap knnResult = GroundTruth.QueryUtil.searchKnn(traj,ExpConfig.kKnn, ExpConfig.tau);

                System.out.println(ExpConfig.kKnn + " " + q + "/" + testSet.size());
                for(int id:knnResult.keys()){
                    double distance = knnResult.get(id);
                    bufferedWriter.write("("+id + ",");
                    bufferedWriter.write(distance + ");");
                }
                bufferedWriter.newLine();
                bufferedWriter.flush();

            }
            bufferedWriter.close();
        }catch (IOException e) {
            e.printStackTrace();
        }


    }
}
