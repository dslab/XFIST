package Flood;

import gnu.trove.list.array.TDoubleArrayList;
import gnu.trove.list.array.TIntArrayList;
import learnedIndex.LinearRegression;
import learnedIndex.MultiLearnedIndex;
import learnedIndex.SortedArray;

import java.io.Serializable;
import java.util.List;

public class FloodIndex extends MultiLearnedIndex implements Serializable {
    //default serialVersion id
    private static final long serialVersionUID = 1L;

    /*Structure of flood index:
    ---------------------------
    nonSortedDimTails: locates dim-1 cells
    suppose 3x3 cells
    8 ----------
      |  |  |  |
    4 ----------
      |  |  |  |
    1 ----------
      |  |  |  |
      ----------
      3  6  7  10
    nonSortedDimTails will contain last values of each dimension separation
    nonSortedDimTails[0] = {6,7,10}
    nonSortedDimTails[1] = {1,4, 8}


    sortedDim --> learned sorted dimension by lrSets

    lrSet[j] contains linear regressions in the flood index at cell j to index sortedDim of data
    lrHeadSet[j] contains the first appropriate key of the lin regressions at cells [j]


     */


    private TDoubleArrayList[] nonSortedDimTails;
    private int sortedDim;

    public FloodIndex(TDoubleArrayList[] nonsortedDimHead, int sortedDim, SortedArray array,
                      List<LinearRegression>[] lrSet, TDoubleArrayList[] lrHeadSet, TIntArrayList lastPositionInBin){
        this.setNonSortedDimTails(nonsortedDimHead);
        this.setSortedDim(sortedDim);
        this.setArray(array);
        this.setLrSetArray(lrSet);
        this.setLrHeadSet(lrHeadSet);
        this.setLastPositionInBin(lastPositionInBin);

    }

    public void setLastPositionInBin(TIntArrayList lastPositionInBin){
        this.lastPositionInPartition = lastPositionInBin;
    }

    public int getFirstPositionAtBin(int iBin){
        if(iBin == 0)
            return 0;
        return lastPositionInPartition.get(iBin-1)+1;
    }

    public int getLastPositionAtBin(int iBin){
        return lastPositionInPartition.get(iBin);
    }


    public TDoubleArrayList[] getNonSortedDimTails() {
        return nonSortedDimTails;
    }

    public void setNonSortedDimTails(TDoubleArrayList[] nonSortedDimTails) {
        this.nonSortedDimTails = nonSortedDimTails;
    }

    public int getSortedDim() {
        return sortedDim;
    }

    public void setSortedDim(int sortedDim) {
        this.sortedDim = sortedDim;
    }
}
