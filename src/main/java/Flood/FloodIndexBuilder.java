package Flood;

import Utils.Util;
import gnu.trove.list.array.TDoubleArrayList;
import gnu.trove.list.array.TIntArrayList;
import learnedIndex.LinearRegression;
import learnedIndex.LrSetBuilder;
import learnedIndex.SortedArray;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FloodIndexBuilder {
    private static TDoubleArrayList[] createPartitions(List<double[]> points,int choice, int[] bins,
                                                       int dim){
        TDoubleArrayList[] values = new TDoubleArrayList[dim-1];

        //partitioning the unsorted dimensions REF1
        for(int j=0;j<dim;j++) {
            if(j==choice)
                continue;
            int largerThanChoice = (j>=choice)? 1: 0;
            int jj = j - largerThanChoice;
            values[jj] = new TDoubleArrayList();
            for (int i = 0; i < points.size(); i++) {
                values[jj].add(points.get(i)[j]);
            }
            values[jj].sort();
        }
        TDoubleArrayList[] binTails = new TDoubleArrayList[dim-1];
        int numBins = 1;


        //creating partitions as many as bins: numPartitions = bins[0] *bins[1] * bins[2] ... * bins[X]
        for(int j=0;j<dim-1;j++) {
            binTails[j] = new TDoubleArrayList();
            for (int i = 0; i < bins[j]; i++) {

                int ii = (i+1) * points.size() / bins[j];
                if(ii==points.size())
                    ii--;
                binTails[j].add(values[j].get(ii));
            }
            numBins = numBins*bins[j];
        }
        return binTails;
    }


    private static List<Util.IntDblPair>[]
    collectPointToPartitions(List<double[]> points, TIntArrayList pointers,
                             int choice, int[] bins, TDoubleArrayList[] binTails,
                             int dim){



        int numBins = 1;
        for(int i=0;i<bins.length;i++){
            numBins = numBins * bins[i];
        }

        //put points to their respective partitions as described by the partitioning (REF1)
        List<Util.IntDblPair>[] partitions = new List[numBins];

        for(int j=0;j<numBins;j++)
            partitions[j] = new ArrayList<>();

        for (int i = 0; i < points.size(); i++) {
            int iBin = 0;
            for(int j=0;j<dim;j++){
                if(j==choice)
                    continue;

                int largerThanChoice = (j>=choice)? 1: 0;
                int jj = j - largerThanChoice;

                int cBin = Util.findLessBoundary(binTails[jj],points.get(i)[j]);

                if(cBin < 0)
                    cBin = 0;

                if(cBin == bins[jj])
                    cBin--;

                if(jj==0)
                    iBin = cBin;
                else
                    iBin = (iBin * bins[jj]) + cBin;
            }
            partitions[iBin].add(new Util.IntDblPair(pointers.get(i), points.get(i)[choice]));
        }

        return partitions;
    }

    public static FloodIndex buildFloodIndex(List<double[]> points, TIntArrayList pointers, double errorThreshold,
                                             int choice, int[] bins) {
        //points: list of indexed points
        //pointers: pointer of a point to the sub-traj table
        //          (+) sign means it is a MAX point
        //          (-) sign means it is a MIN point
        //errorThreshold: the tolerance of error allowed by the learned index misprediction
        //choice: choice of selected dimension to be sorted/indexed
        //bins: configuration of partitioning
        int dim = points.get(0).length;

        TDoubleArrayList[] binTails = createPartitions(points, choice, bins, dim);
        List<Util.IntDblPair>[] partitions = collectPointToPartitions(points, pointers, choice, bins, binTails, dim);


        //init things
        TDoubleArrayList indices = new TDoubleArrayList(); //indices as learn target (Y)
        TDoubleArrayList keys = new TDoubleArrayList(); //key as learn predictor (X)
        TIntArrayList pointerx = new TIntArrayList();// for sorted array
        TIntArrayList lastPositionInBins = new TIntArrayList();//to identify if a key should be inside the partition
        // (for prediction/search)

        LrSetBuilder lrSetBuilder = new LrSetBuilder();
        List<LinearRegression>[] lrSet = new ArrayList[partitions.length];
        TDoubleArrayList[] lrHeadSet = new TDoubleArrayList[partitions.length];



        int cur = 0;
        for (int i = 0; i < partitions.length; i++) {
            //if the partition is empty, skip learning and set the:
            // lastPositionInBins as the same as previous bin
            // linear regressions for that bin as empty
            // linear regression head set (to identify is that key should be predicted with a LR) as empty
            if(partitions[i].size()==0){
                lastPositionInBins.add(pointerx.size()-1);
                lrSet[i] = null;
                lrHeadSet[i] = null;
                continue;
            }

            //sort by double value (key)
            Collections.sort(partitions[i]);

            for(int j=0;j<partitions[i].size();j++){
                indices.add(cur);//add current index as target
                pointerx.add(partitions[i].get(j).intVal);//add pointer following the sorted key order (for sorted array later)
                keys.add((float)partitions[i].get(j).dblVal);//add key following the sorted key order (for sorted array later)
                cur++; //increase index
            }


            lastPositionInBins.add(cur-1);//add last position (current index) in current bin

            lrSetBuilder.createTrainDataset(keys.subList(cur-partitions[i].size(),cur),
                    indices.subList(cur-partitions[i].size(),cur));
            //create dataset using subList of Trove based data structures
            // (not partitions) because: the dataset uses trove
            // intDoublePair does not provide subList

            //learn the f:X --> Y with X,Y,and errorThreshold
            lrSetBuilder.buildLrSetv2(errorThreshold);
            lrSet[i] = lrSetBuilder.getLrSet();
            lrHeadSet[i] =lrSetBuilder.getLrSetFirstValues();
        }

        SortedArray array = new SortedArray();
        array.setKeys(keys);
        array.setPtr(pointerx);

        FloodIndex index = new FloodIndex(binTails,choice,array,lrSet,lrHeadSet,lastPositionInBins);
        return index;
    }
}
