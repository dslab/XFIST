package Flood;

import gnu.trove.list.array.TIntArrayList;

import java.util.List;
import java.util.Random;

public class FloodGroupBuilder {


    private static final int MIN = 0;
    private static final int MAX = 1;
    private static final int FIRST = 2;
    private static final int LAST = 3;
    private static final int SUBMIN = 4;
    private static final int SUBMAX = 5;
    private static final int SINGLES = 6;

    private static final int PTR_MIN = 0;
    private static final int PTR_MAX = 0;
    private static final int PTR_FIRST = 0;
    private static final int PTR_LAST = 0;
    private static final int PTR_SUB = 1;
    private static final int PTR_SINGLES = 2;

    private static double randMean = 10;
    private static double randDev = 5;

    private static int partitionScheme[][];
    private static int nPartition = 0;
    public static void setNPartition(int fixedBinNum){
        nPartition = fixedBinNum;
    }

    public static void configureRandomParams(double randMean_, double randDev_){
        randMean = randMean_;
        randDev = randDev_;
    }


    public static FloodIndicesGroup build(List<List> pointCollection, List<TIntArrayList> ptrCollection,
                             int dim, double errThreshold){

        int indexNum = pointCollection.size();
        //partitionScheme will have size (indexNum + 1)*[]
        //0th index --> dimension of choice --> [size: indexNum]
        //the rest --> [size: dim-1]
        if(nPartition == 0){
            partitionScheme = createRandompartitions(indexNum, dim);
        }
        else
            partitionScheme = createFixedPartitions(indexNum, dim);

        FloodIndicesGroup floodIndicesGroup = new FloodIndicesGroup();

        int[] indexType = {MIN,MAX,FIRST,LAST,SUBMIN,SUBMAX,SINGLES};
        int[] ptrType = {PTR_MIN,PTR_MAX,PTR_FIRST,PTR_LAST,PTR_SUB,PTR_SUB,PTR_SINGLES};

        for(int i=0;i<indexNum;i++){
            FloodIndex index = FloodIndexBuilder.buildFloodIndex(pointCollection.get(indexType[i]),
                    ptrCollection.get(ptrType[i]),errThreshold,partitionScheme[0][i],
                    partitionScheme[i+1]);
            floodIndicesGroup.register(index,indexType[i]);

        }
        return floodIndicesGroup;
    }

    private static int[][] createRandompartitions(int indexNum, int dim) {
        int[][] partitions = new int[indexNum+1][];
        Random r = new Random();
        int[] choices = new int[indexNum];
        for(int i=0;i<indexNum;i++){
            choices[i] = r.nextInt(dim);
        }
        partitions[0] = choices;
        for(int i=0;i<indexNum;i++){
            partitions[i+1] = new int[dim-1];
            for(int j=0;j<dim-1;j++){
                double value = (r.nextGaussian() * randDev) * randMean;
                if(value<1)
                    value = 1;
                partitions[i+1][j] = (int) value;
            }
        }
        return partitions;
    }

    private static int[][] createFixedPartitions(int indexNum, int dim){
        int[][] partitions = new int[indexNum+1][];

        int[] choices = new int[indexNum];
        for(int i=0;i<indexNum;i++){
            choices[i] = i%dim;
        }
        partitions[0] = choices;
        for(int i=0;i<indexNum;i++){
            partitions[i+1] = new int[dim-1];
            for(int j=0;j<dim-1;j++){
                partitions[i+1][j] = nPartition;
            }
        }
        return partitions;
    }
}
