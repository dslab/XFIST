package Flood;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FloodIndicesGroup  implements Serializable {
    //default serialVersion id
    private static final long serialVersionUID = 1L;


    public static int MIN = 0;
    public static int MAX = 1;
    public static int FIRST = 2;
    public static int LAST = 3;
    public static int SUBMIN = 4;
    public static int SUBMAX = 5;
    public static int SINGLES = 6;

    public static int SIZE = 7;

    private FloodIndex[] indices;

    public FloodIndicesGroup(){
        indices = new FloodIndex[SIZE];
    }

    public void register(FloodIndex index, int pointType){
        indices[pointType] = index;
    }

    public FloodIndex getIndex(int pointType){
        return indices[pointType];
    }
}
