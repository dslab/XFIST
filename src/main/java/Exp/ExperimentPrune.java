package Exp;

import AllPointFlood.AllPointIndex;
import AllPointFlood.AllPointPruneUtil;
import Evaluation.ExpEvaluateResult;
import XFist.XfistPruneUtil;
import XFist.Xfist;
import Utils.Distance;
import Utils.Logger;
import Utils.TrajUtil;
import gnu.trove.list.TIntList;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class ExperimentPrune {
    private static String logFile;
    private static String indexFilename;
    private static int splitPrune;
    private static String distanceString;


    public static void prepareParams() {


        //public static String logFilePrefixPrune = "log/prune/LOG_FILE_PRUNE_";
        //set the log file name prefix
        //final log file name: LOG_FILE_PRUNE_[indextype]_[datasetName]_[splitBuild]_
        // [errorThresholdFactor]_[partitionScheme]_[date (yyyyMMddhhmmss)].log

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        String current = dateFormat.format(new Date());

        splitPrune = ExpConfig.splitPrune;
        switch (ExpConfig.distanceType){
            case "FRECHET":
                Distance.setCurrentDistance(Distance.FRECHET);
                Distance.setParameters(-1,-1);
                distanceString = ExpConfig.distanceType + " " + String.format("%.3f",ExpConfig.tau);
                break;
            case "DTW":
                Distance.setCurrentDistance(Distance.DTW);
                Distance.setParameters(-1,-1);
                distanceString = ExpConfig.distanceType + " " + String.format("%.3f",ExpConfig.tau);
                break;

            case "EDR":
                Distance.setCurrentDistance(Distance.EDR);
                Distance.setEps(ExpConfig.eps);
                distanceString = ExpConfig.distanceType + " " +  String.format("%.1f",ExpConfig.tau)+" "+ String.format("%.3f",ExpConfig.eps);
                break;
            case "LCSS":
                Distance.setCurrentDistance(Distance.LCSS);
                Distance.setEps(ExpConfig.eps);
                Distance.setSpaceConstraint(ExpConfig.spaceConstraint);
                distanceString = ExpConfig.distanceType + " " +  String.format("%.1f",ExpConfig.tau)+" "+ String.format("%.3f",ExpConfig.eps)+" "+ExpConfig.spaceConstraint;
                break;
        }
        indexFilename = ExpConfig.indexFilename;
        logFile = ExpConfig.expPathDefault+ExpConfig.logFilePrefixPrune + indexFilename + "_"+splitPrune+"_"+distanceString+"_"+current + ".log";
        String headers = "test num,indexType,datasetName,indexFilename,splitPrune,distanceType,tau,eps,spaceConstraint,total prune time (ms)";
        Logger.createHeaderFile(ExpConfig.expPathDefault+ExpConfig.logFilePrefixPrune+"HEADER",headers);

        TrajUtil.loadTrajDatasetFromFile(ExpConfig.expPathDefault+ExpConfig.testPath+
                ExpConfig.testFile);

    }

    public static void run(){
        Logger.startLog(logFile);
        System.out.println("Running pruning with index: "+indexFilename);
        int trSize = TrajUtil.trajectoryDataset.size();
        for(int i=0;i<ExpConfig.repeat;i++) {

            double pruneTime = 0;
            try {
                File pruneFile = new File(ExpConfig.expPathDefault+ExpConfig.prunedPath+"pruned_"+indexFilename+"_"+splitPrune+"_"+distanceString);
                FileOutputStream fos = new FileOutputStream(pruneFile);
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(fos));
                Xfist index = null;
                AllPointIndex allPointIndex = null;
                switch (ExpConfig.indexType) {
                    case "SUBTRAJFLOOD":
                        index = (Xfist) ReadObjectFromFile(ExpConfig.expPathDefault+ExpConfig.indexPath + ExpConfig.indexFilename+ExpConfig.indexFileExt);
                        break;

                    case "ALLPOINTFLOOD":
                        allPointIndex = (AllPointIndex) ReadObjectFromFile(ExpConfig.expPathDefault+ExpConfig.indexPath + ExpConfig.indexFilename+ExpConfig.indexFileExt);
                        break;
                }
                System.out.println("Running pruning: "+distanceString);
                for (int t = 0; t < trSize; t++) {

                    List<double[]> qTraj = TrajUtil.trajectoryDataset.get(t);

                    long searchStart = System.currentTimeMillis();
                    TIntList res = null;
                    switch (ExpConfig.indexType) {
                        case "SUBTRAJFLOOD":
                            res = XfistPruneUtil.prune(index, qTraj, ExpConfig.tau, splitPrune);
                            break;

                        case "ALLPOINTFLOOD":
                            res = AllPointPruneUtil.prune(allPointIndex, qTraj, ExpConfig.tau);
                            break;
                    }
                    pruneTime = pruneTime + (System.currentTimeMillis() - searchStart);

                    String resString = res.toString();
                    resString=resString.substring(1,resString.length()-1);

                    bufferedWriter.write(resString);
                    bufferedWriter.newLine();
                    bufferedWriter.flush();

                    ExpEvaluateResult.computeMetricsWithTestI(res,t);

                }
                //"test num,indexType,datasetName,indexFilename,splitPrune,distanceType,tau,eps,spaceConstraint,total prune time (ms)";
                Logger.concatLine(i+",");
                Logger.concatLine(ExpConfig.indexType+",");
                Logger.concatLine(ExpConfig.datasetFilename+",");
                Logger.concatLine(ExpConfig.indexFilename+",");
                Logger.concatLine(splitPrune+",");
                Logger.concatLine(ExpConfig.distanceType+",");
                Logger.concatLine(ExpConfig.tau+",");
                Logger.concatLine(Distance.eps+",");
                Logger.concatLine(Distance.spaceConstraint+",");
                Logger.concatLine(String.valueOf(pruneTime));
                Logger.writeLine();

                fos.close();
                bufferedWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        Logger.endLog();
    }
    public static Object ReadObjectFromFile(String filepath) {

        try {

            FileInputStream fileIn = new FileInputStream(filepath);
            ObjectInputStream objectIn = new ObjectInputStream(fileIn);

            Object obj = objectIn.readObject();

            System.out.println("Index from file "+filepath+" is loaded");
            objectIn.close();
            return obj;

        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
