package Exp;

import Demo.Cmd;
import Evaluation.ExpEvaluateResult;
import GroundTruth.ExperimentGroundTruth;
import RawDataFormatter.ChengduFormatter;
import RawDataFormatter.PortoFormatter;
import Utils.*;
import org.yaml.snakeyaml.Yaml;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class ExpMain {

    static Map<String, Object> yamlConfig;

    public static void main(String[] args) {

        //readConfig

        switch(args[0]){
            case "DESCRIBE":
                loadYamlConfig(args[1]);
                loadPrepareParam();
                prepareDataset();
                describeDataset();
                break;
//            case "DATA_GENERATE":
//                DataGenerator.main(args);
//                break;
            case "CHENGDU_FORMATTER":
                loadYamlConfig(args[1]);
                ChengduFormatter.main(args);
                break;
            case "PORTO_FORMATTER":
                loadYamlConfig(args[1]);
                PortoFormatter.main(args);
                break;
            case "CREATE_TEST":
                loadYamlConfig(args[1]);
                loadCreateTestParam();
                ExpCreateTest.main(args);
                break;
            case "BUILD":
                loadYamlConfig(args[1]);
                loadPrepareParam();
                loadBuildParam();
                List<Double> sampleSizeList = readListConfig("sampleSizeList");
                List<String> errorThresholdFactorList = readListConfig("errorThresholdFactorList");
                List<Integer> splitBuildList= readListConfig("splitBuildList");
                List<String> partitionSchemeList = readListConfig("partitionSchemeList");
                for(double sampleSize:sampleSizeList) {
                    ExpConfig.sampleSize = sampleSize;
                    prepareDataset();
                    for(String errorThresholdFactor:errorThresholdFactorList) {
                        ExpConfig.errorThresholdFactor = errorThresholdFactor;
                        for (String partitionScheme : partitionSchemeList) {
                            ExpConfig.partitionScheme = partitionScheme;
                            for (int splitBuild : splitBuildList) {
                                ExpConfig.splitBuild = splitBuild;
                                ExperimentBuild.prepareParams();
                                ExperimentBuild.run();
                            }
                        }
                    }
                }
                break;
            case "PRUNE":
                loadYamlConfig(args[1]);
                loadPruneParam();
                List<String> indexFilenameList = readListConfig("indexFilenameList");
                List<String> distanceTypeList= readListConfig("distanceTypeList");
                List<Double> tauList= readListConfig("tauList");
                List<Double> epsList= readListConfig("epsList");
                List<Integer> spaceConstraintList= readListConfig("spaceConstraintList");
                List<Integer> splitPruneList= readListConfig("splitPruneList");

                for(String indexFilename:indexFilenameList) {
                    ExpConfig.indexFilename = indexFilename;
                    for (Integer splitPrune : splitPruneList) {
                        ExpConfig.splitPrune = splitPrune;
                        for (String distanceType : distanceTypeList) {
                            ExpConfig.distanceType = distanceType;
                            for (double tau : tauList) {
                                ExpConfig.tau = tau;
                                ExpEvaluateResult.init();
                                ExpEvaluateResult.getFilteredGroundTruth(ExpConfig.groundTruthPath+
                                        ExpConfig.groundTruthFile);
                                if (distanceType.equals("DTW") || distanceType.equals("FRECHET")) {
                                    ExperimentPrune.prepareParams();
                                    ExperimentPrune.run();
                                } else {
                                    for (double eps : epsList) {
                                        ExpConfig.eps = eps;
                                        if (distanceType.equals("LCSS")) {
                                            for (Integer spaceConstraint : spaceConstraintList) {
                                                ExpConfig.spaceConstraint =spaceConstraint ;
                                                ExperimentPrune.prepareParams();
                                                ExperimentPrune.run();
                                            }
                                        } else {
                                            ExperimentPrune.prepareParams();
                                            ExperimentPrune.run();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                break;
            case "QUERY_SIM":
                loadYamlConfig(args[1]);
                loadQueryParam();
                List<String> prunedFilesList = readListConfig("prunedFilesList");
                for(String prunedFile:prunedFilesList) {
                    ExpConfig.prunedFile = prunedFile;
                    ExperimentQuery.prepareParams();
                    ExpEvaluateResult.init();
                    ExperimentQuery.run();
                }
                break;

            case "QUERY_KNN":
                loadYamlConfig(args[1]);
                loadQueryParam();
                prunedFilesList = readListConfig("prunedFilesList");
                for(String prunedFile:prunedFilesList) {
                    ExpConfig.prunedFile = prunedFile;
                    ExperimentQuery.prepareParams();
                    ExpEvaluateResult.init();
                    ExperimentQuery.run();
                }
                break;
            case "GROUND_TRUTH_QUERY":
                loadYamlConfig(args[1]);
                loadQueryParam();
                ExperimentGroundTruth.prepareParams();
                ExperimentGroundTruth.runSearchSim();
                break;
            case "GROUND_TRUTH_KNN":
                loadYamlConfig(args[1]);
                loadQueryParam();
                ExperimentGroundTruth.prepareParams();
                ExperimentGroundTruth.runSearchKnn();
                break;

            case "EVALUATE_PRUNE":
                loadYamlConfig(args[1]);
                loadQueryParam();
                ExpEvaluateResult.init();
                ExpEvaluateResult.getPruned(ExpConfig.expPathDefault+ExpConfig.prunedPath + ExpConfig.prunedFile);
                ExpEvaluateResult.getFilteredGroundTruth(ExpConfig.expPathDefault+ExpConfig.groundTruthPath+
                        ExpConfig.groundTruthFile);
                ExpEvaluateResult.evaluate();
                break;

            case "DEMO":
                Cmd.main(Arrays.copyOfRange(args,1,args.length));
                break;
        }
    }

    public static List readListConfig(String stringConfig){
        List listConfig;
        if(yamlConfig.get(stringConfig) != null) {
            listConfig =
                    (List) yamlConfig.get(stringConfig);
        }
        else{
            listConfig = new ArrayList<>();
            switch(stringConfig){
                case "sampleSizeList":
                    listConfig.add(ExpConfig.sampleSize);
                    break;
                case "errorThresholdFactorList":
                    listConfig.add(ExpConfig.errorThresholdFactor);
                    break;

                case "partitionSchemeList":
                    listConfig.add(ExpConfig.partitionScheme);
                    break;
                case "indexFilenameList":
                    listConfig.add( ExpConfig.indexFilename);
                    break;
                case "splitBuildList":
                    listConfig.add(ExpConfig.splitBuild);
                    break;
                case "splitPruneList":
                    listConfig.add(ExpConfig.splitPrune);
                    break;
                case "distanceTypeList":
                    listConfig.add(ExpConfig.distanceType);
                    break;
                case "tauList":
                    listConfig.add(ExpConfig.tau);
                    break;
                case "epsList":
                    listConfig.add(ExpConfig.eps);
                    break;
                case "spaceConstraintList":
                    listConfig.add(ExpConfig.spaceConstraint);
                    break;
                case "prunedFilesList":
                    listConfig.add(ExpConfig.prunedFile);
                    break;
            }

        }
        return listConfig;
    }

    public static void prepareDataset(){
        TrajUtil.loadTrajDatasetFromFile(ExpConfig.datasetPathDefault+ExpConfig.datasetFilename+ExpConfig.datasetFileExt);
        int trSize = (int) Math.ceil(TrajUtil.trajectoryDataset.size() * ExpConfig.sampleSize);
        TrajUtil.trajectoryDataset = TrajUtil.trajectoryDataset.subList(0,trSize);
        System.out.println("Dataset "+ExpConfig.datasetFilename+" with sample size of "+ExpConfig.sampleSize+" loaded");

    }

    public static void loadYamlConfig(String yamlFileConfig){


        Yaml yamlParser = new Yaml();
        InputStream inputStream = null;
        try {
            inputStream = new FileInputStream(yamlFileConfig);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        yamlConfig= yamlParser.load(inputStream);

    }

    public static void loadPrepareParam(){
        ExpConfig.datasetPathDefault = (String) yamlConfig.get("datasetPathDefault");
        ExpConfig.datasetFilename = (String) yamlConfig.get("datasetFilename");
        ExpConfig.datasetFileExt = (String) yamlConfig.get("datasetFileExt");
        ExpConfig.sampleSize = (Double) yamlConfig.get("sampleSize");
    }

    public static void loadBuildParam(){
        ExpConfig.datasetFileExt=(String)yamlConfig.get("datasetFileExt");
        ExpConfig.datasetFilename=(String)yamlConfig.get("datasetFilename");
        ExpConfig.errorThresholdFactor=(String)yamlConfig.get("errorThresholdFactor");
        ExpConfig.expPathDefault=(String)yamlConfig.get("expPathDefault");
        ExpConfig.indexFileExt=(String)yamlConfig.get("indexFileExt");
        ExpConfig.indexPath=(String)yamlConfig.get("indexPath");
        ExpConfig.indexType=(String)yamlConfig.get("indexType");
        ExpConfig.logFilePrefixBuild=(String)yamlConfig.get("logFilePrefixBuild");
        ExpConfig.partitionScheme=(String)yamlConfig.get("partitionScheme");
        ExpConfig.repeat=(Integer)(yamlConfig.get("repeat"));
        ExpConfig.splitBuild=(Integer)(yamlConfig.get("splitBuild"));

    }

    public static void loadCreateTestParam(){
        ExpConfig.datasetFileExt=(String)yamlConfig.get("datasetFileExt");
        ExpConfig.datasetFilename=(String)yamlConfig.get("datasetFilename");
        ExpConfig.datasetPathDefault=(String)yamlConfig.get("datasetPathDefault");
        ExpConfig.expPathDefault=(String)yamlConfig.get("expPathDefault");
        ExpConfig.testPath=(String)yamlConfig.get("testPath");

    }

    public static void loadPruneParam(){
        ExpConfig.datasetFilename=(String)yamlConfig.get("datasetFilename");
        ExpConfig.distanceType=(String)yamlConfig.get("distanceType");
        ExpConfig.eps=(Double)yamlConfig.get("eps");
        ExpConfig.expPathDefault=(String)yamlConfig.get("expPathDefault");
        ExpConfig.indexFileExt=(String)yamlConfig.get("indexFileExt");
        ExpConfig.indexFilename=(String)yamlConfig.get("indexFilename");
        ExpConfig.indexPath=(String)yamlConfig.get("indexPath");
        ExpConfig.indexType=(String)yamlConfig.get("indexType");
        ExpConfig.logFilePrefixPrune =(String)yamlConfig.get("logFilePrefixPrune");
        ExpConfig.prunedPath=(String)yamlConfig.get("prunedPath");
        ExpConfig.repeat=(Integer)yamlConfig.get("repeat");
        ExpConfig.spaceConstraint=(Integer)yamlConfig.get("spaceConstraint");
        ExpConfig.splitPrune=(Integer)yamlConfig.get("splitPrune");
        ExpConfig.tau=(Double)yamlConfig.get("tau");
        ExpConfig.testFile=(String)yamlConfig.get("testFile");
        ExpConfig.testPath=(String)yamlConfig.get("testPath");
        ExpConfig.groundTruthPath=(String) yamlConfig.get("groundTruthPath");
        ExpConfig.groundTruthFile=(String)yamlConfig.get("groundTruthFile");

    }

    public static void loadQueryParam(){
        ExpConfig.datasetFileExt=(String)yamlConfig.get("datasetFileExt");
        ExpConfig.datasetFilename=(String)yamlConfig.get("datasetFilename");
        ExpConfig.datasetPathDefault=(String)yamlConfig.get("datasetPathDefault");
        ExpConfig.distanceType=(String)yamlConfig.get("distanceType");
        ExpConfig.eps=(Double)yamlConfig.get("eps");
        ExpConfig.expPathDefault=(String)yamlConfig.get("expPathDefault");
        ExpConfig.indexFilename=(String)yamlConfig.get("indexFilename");
        ExpConfig.indexType=(String)yamlConfig.get("indexType");
        ExpConfig.logFilePrefixQuery=(String)yamlConfig.get("logFilePrefixQuery");
        ExpConfig.prunedFile =(String)yamlConfig.get("prunedFile");
        ExpConfig.prunedPath=(String)yamlConfig.get("prunedPath");
        ExpConfig.kKnn = (Integer)yamlConfig.get("kKnn");
        ExpConfig.repeat=(Integer)yamlConfig.get("repeat");
        ExpConfig.spaceConstraint=(Integer)yamlConfig.get("spaceConstraint");
        ExpConfig.splitPrune=(Integer)yamlConfig.get("splitPrune");
        ExpConfig.tau=(Double)yamlConfig.get("tau");
        ExpConfig.testFile=(String)yamlConfig.get("testFile");
        ExpConfig.testPath=(String)yamlConfig.get("testPath");
        ExpConfig.groundTruthPath=(String) yamlConfig.get("groundTruthPath");
        ExpConfig.groundTruthFile=(String)yamlConfig.get("groundTruthFile");
    }

    public static void describeDataset(){
        System.out.println(ExpConfig.datasetFilename);
        System.out.println(TrajUtil.trajectoryDataset.size());
        int sumLength = 0;
        int minLength = 9999;
        int maxLength = 0;
        int dim = TrajUtil.trajectoryDataset.get(0).get(0).length;
        double maxPoint[] = new double[dim];
        double meanPoint[] = new double[dim];

        double minPoint[] = new double[dim];
        for(int i=0;i<dim;i++){
            maxPoint[i] = -999999.999;
            minPoint[i] = 999999.999;
            meanPoint[i] = 0;
        }

        for(int i=0;i<TrajUtil.trajectoryDataset.size();i++){
            if(i%25000 == 0)
                System.out.println(i+"/"+TrajUtil.trajectoryDataset.size());
            List<double[]> traj =TrajUtil.trajectoryDataset.get(i);
            sumLength = sumLength+traj.size();
            if(traj.size()>maxLength)
                maxLength = traj.size();
            if(traj.size()<minLength)
                minLength = traj.size();
            for(int j=0;j<traj.size();j++){
                for(int k=0;k<dim;k++){
                    if(maxPoint[k]<traj.get(j)[k])
                        maxPoint[k]=traj.get(j)[k];

                    if(minPoint[k]>traj.get(j)[k])
                        minPoint[k]=traj.get(j)[k];

                    meanPoint[k] +=traj.get(j)[k];
                }
            }
        }

        System.out.print("MIN: ");
        for(int i=0;i<dim;i++){
            System.out.print( minPoint[i]+",");
        }
        System.out.println();
        System.out.print("MAX: ");
        for(int i=0;i<dim;i++){
            System.out.print(maxPoint[i]+",");
        }
        System.out.print("MEAN: ");
        for(int i=0;i<dim;i++){
            System.out.print(meanPoint[i]+",");
        }
        System.out.println();
        System.out.println("AvgLength: "+sumLength/TrajUtil.trajectoryDataset.size());

    }

}
