package Exp;

import Utils.TrajUtil;
import Utils.Trajectory;
import gnu.trove.list.array.TIntArrayList;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class ExpCreateTest {
    static int numTest = 1000;
    static int repeat  = 5;
    static HashMap<String,Integer> trajectoryLengths;
    public static void main(String[] args) {
        /*
        String[] datasetFilenames = {"chengdu.csv","atc-x.csv","porto.csv","atc-1000.csv","chengdu-tiny.csv"};
        String[] datasetNames = {"chengdu","atc","porto","atc-1000","chengdu-tiny"};
        int[] numTrajectories = {6832301,2645645,1704769,1000,1000000};
*/
        trajectoryLengths = new HashMap<>();
        trajectoryLengths.put("porto",1704769);
        trajectoryLengths.put("chengdu",2163572);
        trajectoryLengths.put("atc-x-normal",2645645);
        trajectoryLengths.put("atc-1000",1000);
        trajectoryLengths.put("chengdu-tiny",1000000);

        Random r = new Random();
        //TrajUtil.loadTrajDatasetFromFile(ExpConfig.datasetPathDefault+datasetFilenames[s]);
        try {
            for(int rep = 0;rep<repeat;rep++) {
                File testSet = new File(ExpConfig.expPathDefault+
                        ExpConfig.testPath+"test_"+ExpConfig.datasetFilename+"_"+(rep+1));

                int numTrajectories = trajectoryLengths.get(ExpConfig.datasetFilename);
                TIntArrayList randomSet = new TIntArrayList();
                if ( numTrajectories==numTest) {
                    for (int i = 0; i < numTest; i++) {
                        randomSet.add(i);
                    }
                }
                else {
                    for (int i = 0; i < numTest; i++) {
                        int randomVal = r.nextInt(numTrajectories);
                        while (randomSet.contains(randomVal)) {
                            randomVal = r.nextInt(numTrajectories);
                        }
                        randomSet.add(randomVal);
                    }
                    randomSet.sort();
                }

                FileOutputStream fos = new FileOutputStream(testSet);
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(fos));


                File datasetFile = new File(ExpConfig.datasetPathDefault+ExpConfig.datasetFilename+
                        ExpConfig.datasetFileExt);
                FileInputStream fis = new FileInputStream(datasetFile);
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fis));
                String input;
                int p = 0;
                int iterSet = 0;
                while ((input = bufferedReader.readLine()) != null) {
                    if(randomSet.get(iterSet) == p){
                        bufferedWriter.write(input);
                        bufferedWriter.newLine();
                        bufferedWriter.flush();
                        iterSet++;
                        if(iterSet == randomSet.size())
                            break;
                    }
                    p++;
                }

                bufferedWriter.close();
                fos.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    private static String extractStringTrajectory(int t){
        String s = "";
        List<double[]> trajectory = TrajUtil.trajectoryDataset.get(t);
        for(int i=0;i<trajectory.size();i++){
            double[] point = trajectory.get(i);
            for(int d = 0;d<point.length;d++){
                s = s + point[d];
                if(d< point.length-1)
                    s = s + ",";

            }
            if(i< trajectory.size()-1)
                s = s + ";";

        }

        return s;
    }


}
