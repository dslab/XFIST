package Exp;

import Flood.FloodIndex;
import Flood.FloodIndicesGroup;
import Utils.Distance;
import Utils.TrajUtil;
import gnu.trove.list.TIntList;
import gnu.trove.list.array.TDoubleArrayList;
import gnu.trove.list.array.TIntArrayList;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

public  class Experiment {
    static List<Integer> queryIds;
    static int qSize = 1000;
    static int splitNumber = 3;
    static int trSize = 1000;
    static final int nTest = 5;
    static double epsDouble ;
    static List<List> points;
    static int dim;
    static FloodIndicesGroup floodIndices;


    public static void setTestRandom1000(){

    }

    public static void setTestMini(){
        queryIds = new ArrayList();
        for(int i=0;i<qSize;i++)
            queryIds.add(i);
    }

    public static void setTrainFull(){
        trSize = TrajUtil.trajectoryDataset.size();
    }

    public static void setTrainMini(){
        trSize = 1000;
    }
    public static void run(){
        String epsStr;
        epsDouble = 0.001;


        long pruneStart ;
        double pruneTime = 0;
        long queryStart;
        double queryTime = 0;
        for(;epsDouble<=0.005;epsDouble+=0.001) {
//            int failed = 0;
//            TDoubleArrayList precisions = new TDoubleArrayList();
//            epsStr = String.valueOf(epsDouble);
//            double avgPrecision = 0;
//
//            for(int q:queryIds){
//                pruneStart = System.nanoTime();
//                List<double[]> qTraj = TrajUtil.trajectoryDataset.get(q);
//                TIntList result = Search.SearchUtil.search(floodIndices,qTraj,epsDouble,false,
//                        Distance.currentDistance);
//
//                if(result.size() != 0) {
//                    pruneTime += System.nanoTime() - pruneStart;
//                }
//
//                queryStart = System.nanoTime();
//                double trueSize = 0;
//                for (int i = 0; i < result.size(); i++) {
//                    List<double[]> iTraj = TrajUtil.trajectoryDataset.get(result.get(i));
//                    double dist = TrajUtil.computeDistance(qTraj, iTraj);
//                    if (dist <= epsDouble) {
//                        trueSize++;
//                    }
//                }
//                if(result.size()>0) {
//                    avgPrecision += (trueSize / result.size());
//                    precisions.add(trueSize / result.size());
//                }
//
//                if(result.size() == 0) {
//                    failed++;
//                }
//                else{
//                    queryTime += System.nanoTime() - queryStart;
//                }
//            }
//            //System.out.print(epsStr + " avg precision: "+(avgPrecision/qSize));
//            //System.out.println(epsStr + "avg prune time: "+ (pruneTime/qSize));
//            //System.out.println(epsStr + "avg query time: "+ (queryTime/qSize));
//
//            System.out.print(epsStr+", ");
//            System.out.print(String.format("%.3f",pruneTime/qSize)+", ");
//            System.out.print(String.format("%.3f",queryTime/qSize)+", ");
//            System.out.print(failed+", ");
//            System.out.print(String.format("%.3f",avgPrecision/qSize)+", ");
//            precisions.sort();
//            System.out.println(String.format("%.3f",precisions.get(0))+", "+
//                    String.format("%.3f",precisions.get(precisions.size()/4))+", "+
//                    String.format("%.3f",precisions.get(precisions.size()/2))+", "+
//                    String.format("%.3f",precisions.get(precisions.size()*3/4))+", "+
//                    String.format("%.3f",precisions.max()));
        }
    }
    public static void prepare(){
        List<List<double[]>> trajs = TrajUtil.trajectoryDataset;
        List<double []> mins = new ArrayList<>();
        List<double []> maxs = new ArrayList<>();
        List<double []> firsts = new ArrayList<>();
        List<double []> lasts = new ArrayList<>();
        TIntList ids = new TIntArrayList();
        dim = trajs.get(0).get(0).length;

        for(int i=0;i<trSize;i++){
            double xmin = trajs.get(i).get(0)[0];
            double xmax = trajs.get(i).get(0)[0];
            double ymin = trajs.get(i).get(0)[1];
            double ymax = trajs.get(i).get(0)[1];
            for(int j=1;j<trajs.get(i).size();j++){
                if(xmin > trajs.get(i).get(j)[0])
                    xmin = trajs.get(i).get(j)[0];
                if(xmax < trajs.get(i).get(j)[0])
                    xmax = trajs.get(i).get(j)[0];

                if(ymin > trajs.get(i).get(j)[1])
                    ymin = trajs.get(i).get(j)[1];
                if(ymax < trajs.get(i).get(j)[1])
                    ymax = trajs.get(i).get(j)[1];
            }
            mins.add(new double[]{xmin,ymin});
            maxs.add(new double[]{xmax,ymax});

            firsts.add(trajs.get(i).get(0));
            int tail = trajs.get(i).size()-1;
            lasts.add(trajs.get(i).get(tail));
            ids.add(i+1);
        }

        points = new ArrayList<>();
        points.add(mins);
        points.add(maxs);
        points.add(firsts);
        points.add(lasts);
    }

    public static void runFlood(int partitions){
//        int[] bins = new int[dim-1];
//        for(int i=0;i<dim-1;i++){
//            bins[i] = partitions;
//        }

//        long start = System.currentTimeMillis();
//        FloodIndex floodIndexMin = MainDebug.buildIndexFlood(points.get(0),partitions,
//                1, 20, "min");
//        FloodIndex floodIndexMax = MainDebug.buildIndexFlood(points.get(1),partitions,
//                1, 20, "max");
//        FloodIndex floodIndexFirsts = MainDebug.buildIndexFlood(points.get(2),partitions,
//                1, 20, "firsts");
//        FloodIndex floodIndexLasts = MainDebug.buildIndexFlood(points.get(3),partitions,
//                1, 20, "lasts");
//
//
//        floodIndices = new FloodIndicesGroup();
//
//        floodIndices.register(floodIndexMin,FloodIndicesGroup.MIN);
//        floodIndices.register(floodIndexMax,FloodIndicesGroup.MAX);
//        floodIndices.register(floodIndexFirsts,FloodIndicesGroup.FIRST);
//        floodIndices.register(floodIndexLasts,FloodIndicesGroup.LAST);
//        System.out.println("Flood time building "+(System.currentTimeMillis()-start)+" ms");
//        Search.SearchUtil.setDatasetSize(trSize);
        run();
    }


    public static void loadTestSet(){

    }

    public static void createTestSet(){

        Random r = new Random();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-hhmmss");
        Date date = new Date();
        File testSet = new File("test/set-"+dateFormat.format(date));
        String toWrite = "";

        try {
            FileWriter fileWriter = new FileWriter(testSet);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            for(int j=0;j<nTest;j++) {
                queryIds = new ArrayList();
                for (int i = 0; i < qSize; i++) {
                    int ii = r.nextInt(TrajUtil.trajectoryDataset.size());
                    while (queryIds.contains(ii)) {
                        ii = r.nextInt(TrajUtil.trajectoryDataset.size());
                    }
                    queryIds.add(ii);
                    if(i==0)
                        toWrite = toWrite + ii;
                    else
                        toWrite = toWrite + ","+ii;

                }
                bufferedWriter.write(toWrite);
                bufferedWriter.newLine();
                bufferedWriter.flush();
            }
            bufferedWriter.close();

        }catch (IOException e) {
            e.printStackTrace();
        }

    }
}
