package Exp;

import Evaluation.ExpEvaluateResult;
import Utils.Distance;
import Utils.Logger;
import Utils.TrajUtil;
import gnu.trove.list.TIntList;
import gnu.trove.list.array.TIntArrayList;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ExperimentQuery {

    private static String logFile;
    private static String indexFilename;
    private static int splitPrune;
    private static String distanceString;

    private static  List<List<double[]>> testSet;
    public static void prepareParams() {

        //the parameters is based on the pruned file name:
        //pruned_SUBTRAJFLOOD_atc-x-normal_6_CONST 50_FIX 20.iobj_6_DTW 0.001
        //(0) prefix
        //(1) INDEX TYPE
        //(2) dataset name
        //(3) splitSize
        //(4) index param1
        //(5) index param2.iobj
        //(6) querySplit
        //(7) distance type<space>tau or distance type<space>tau<space>epsilon<space>delta
        //* tau always second
        //we only care with indextype, distance type and querySplit/splitPrune

        //public static String logFilePrefixPrune = "log/prune/LOG_FILE_PRUNE_";
        //set the log file name prefix
        //final log file name: LOG_FILE_PRUNE_[indextype]_[datasetName]_[splitBuild]_
        // [errorThresholdFactor]_[partitionScheme]_[date (yyyyMMddhhmmss)].log

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        String current = dateFormat.format(new Date());

        String prunedFileTokens[] = ExpConfig.prunedFile.split("_");
        splitPrune = Integer.parseInt(prunedFileTokens[6]);

        ExpConfig.indexType = prunedFileTokens[1];

        distanceString = prunedFileTokens[7];
        String distanceTokens[] = distanceString.split(" ");
        ExpConfig.distanceType = distanceTokens[0];
        ExpConfig.tau =Double.parseDouble(distanceTokens[1]);

        splitPrune = ExpConfig.splitPrune;
        switch (ExpConfig.distanceType){
            case "FRECHET":
                Distance.setCurrentDistance(Distance.FRECHET);
                Distance.setParameters(-1,-1);
                break;
            case "DTW":
                Distance.setCurrentDistance(Distance.DTW);
                Distance.setParameters(-1,-1);
                break;

            case "EDR":
                Distance.setCurrentDistance(Distance.EDR);
                ExpConfig.eps = Double.parseDouble(distanceTokens[2]);
                Distance.setEps(ExpConfig.eps);
                break;

            case "LCSS":
                Distance.setCurrentDistance(Distance.LCSS);
                ExpConfig.eps = Double.parseDouble(distanceTokens[2]);
                Distance.setEps(ExpConfig.eps);
                ExpConfig.spaceConstraint = Integer.parseInt(distanceTokens[3]);
                Distance.setSpaceConstraint(ExpConfig.spaceConstraint);
                break;
        }
        indexFilename = ExpConfig.indexFilename;
        logFile = ExpConfig.expPathDefault+ ExpConfig.logFilePrefixQuery + indexFilename + "_"+splitPrune+"_"+distanceString+"_"+current + ".log";
        String headers = "test num,indexType,datasetName,prunedFileName,splitSize,splitPrune,distanceType,tau,eps,spaceConstraint,total query time (ms),average precision";
        Logger.createHeaderFile(ExpConfig.expPathDefault+ExpConfig.logFilePrefixQuery+"HEADER",headers);

        TrajUtil.loadTrajDatasetFromFile(ExpConfig.expPathDefault+ExpConfig.testPath+ExpConfig.testFile);
        //deepCopy
        testSet = new ArrayList<>();
        for(int i=0;i<TrajUtil.trajectoryDataset.size();i++){
            List<double[]> testTraj = new ArrayList<>();
            for(int j=0;j<TrajUtil.trajectoryDataset.get(i).size();j++){
                double[] point = new double[TrajUtil.trajectoryDataset.get(i).get(j).length];
                for(int k=0;k<point.length;k++)
                    point[k] = TrajUtil.trajectoryDataset.get(i).get(j)[k];
                testTraj.add(point);
            }
            testSet.add(testTraj);
        }


        TrajUtil.loadTrajDatasetFromFile(ExpConfig.datasetPathDefault+ExpConfig.datasetFilename+ExpConfig.datasetFileExt);

    }

    public static void run(){
        Logger.startLog(logFile);
        System.out.println("Running query  with pruned: "+ExpConfig.prunedFile);

        try {

            for(int i=0;i<ExpConfig.repeat;i++) {

                File prunedFile = new File(ExpConfig.expPathDefault+ExpConfig.prunedPath+ExpConfig.prunedFile);
                FileInputStream fis = new FileInputStream(prunedFile);
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fis));
                System.out.println("Processing test iteration "+i+"/"+ExpConfig.repeat);
                String input;
                int p = 0;
                double queryTime = 0;
                double avgPrecision = 0;
                while ((input = bufferedReader.readLine()) != null) {
                    TIntList res = new TIntArrayList();
                    String[] tokenized = input.split(",");
                    for(int j=0;j<tokenized.length;j++){
                        res.add(Integer.parseInt(tokenized[j].trim()));
                    }

                    if(p>=testSet.size())
                        break;

                    TIntArrayList finalRes = new TIntArrayList();

                    List<double[]> qTraj = testSet.get(p);

                    long searchStart = System.currentTimeMillis();
                    queryTime = queryTime + (System.currentTimeMillis() - searchStart);

                    searchStart = System.currentTimeMillis();
                    for (int iRes = 0; iRes < res.size(); iRes++) {
                        List<double[]> iTraj = TrajUtil.trajectoryDataset.get(res.get(iRes));
                        double dist = TrajUtil.computeDistance(qTraj, iTraj);
                        if (dist <= ExpConfig.tau) {
                            //System.out.println("("+i+","+dist+")");
                            finalRes.add(iRes);
                        }
                    }

                    ExpEvaluateResult.computeMetricsWithTestI(finalRes,p);

                    avgPrecision = avgPrecision + ((1.0 * finalRes.size()) / res.size());
                    queryTime = queryTime + (System.currentTimeMillis() - searchStart);
                    p++;
                }
                // "test num,indexType,datasetName,prunedFileName,splitSize,splitPrune,distanceType,tau,
                // eps,spaceConstraint,total query time (ms),average precision"
                Logger.concatLine(i+",");
                Logger.concatLine(ExpConfig.indexType+",");
                Logger.concatLine(ExpConfig.datasetFilename+",");
                Logger.concatLine(ExpConfig.prunedFile+",");
                Logger.concatLine(ExpConfig.splitBuild+",");
                Logger.concatLine(splitPrune+",");
                Logger.concatLine(ExpConfig.distanceType+",");
                Logger.concatLine(ExpConfig.tau+",");
                Logger.concatLine(Distance.eps+",");
                Logger.concatLine(Distance.spaceConstraint+",");
                Logger.concatLine((queryTime)+",");
                Logger.concatLine(String.valueOf(avgPrecision/testSet.size()));
                Logger.writeLine();

                fis.close();
                bufferedReader.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


        Logger.endLog();
    }
}
