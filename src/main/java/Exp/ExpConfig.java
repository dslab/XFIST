package Exp;

import java.util.List;

public class ExpConfig {
    public static String expPathDefault = "exp/";
    public static String datasetPathDefault = "../dataset/";
    //atc-subset-f-1000
    //public static String datasetFilename = "chengdu-tiny.csv";
    //public static String datasetName = "chengdu-tiny";
    public static String datasetFilename = "atc-1000";
    public static String datasetFileExt = ".csv";

    public static int atcSize = 1666753;
    public static int portoSize = 1704769;
    public static int chengduSize = 2163572;


    public static double sampleSize = 1.0;
    public static String mode = "QUERY"; //available modes: "BUILD","PRUNE","INDEX","CREATE_TEST"
    //default values: test-[datasetName]-[1...5]

    //parameters for building index
    public static String errorThresholdFactor = "LOG 2"; //
    // available values: LOG x; MUL x; CONST x
    // LOG x: set the PLM index building error threshold to log(|num trajectories|) base x
    // MUL x: set the PLM index building error threshold to x times |num trajectories|, 0<x<1
    // CONST x: set the PLM index building error to constant x
    public static String partitionScheme = "FIX 3";//
    //available values: FIX x, RANDOM x y
    // FIX x: set the partitions of each dimension of flood index to x
    //        can be applied for MLI to set the cluster number to x
    // RANDOM x y: set the partitions of each dimension of flood index to a
    //             random value with mean x and stdev y
    public static int repeat = 5;
    //repeat to get statistical summary/confidence
    public static int splitBuild = 6;
    //available values: x (int)
    // set the sub-trajectory splitting to x
    public static String logFilePrefixBuild = "log/build/LOG_FILE_BUILD_";
    //set the log file name prefix
    //final log file name: LOG_FILE_BUILD_[indextype]_[datasetName]_[splitBuild]_
    // [errorThresholdFactor]_[partitionScheme]_[date (yyyyMMddhhmmss)].log
    public static String indexPath = "index/";

    //parameters for pruning
    public static String testPath = "test/";
    public static String testFile = "test_atc-1000_1";
    public static String indexFileExt = ".iobj";
    public static String indexType = "SUBTRAJFLOOD";
    // !shared with query
    //available values SUBTRAJFLOOD, ALLPOINT, SUBTRAJMLI
    public static String indexFilename = "SUBTRAJFLOOD_atc-1000_4_LOG 2_FIX 3";
    // !shared with query
    //index used to prune the dataset when tested
    //structure: [indextype]_[datasetName]_[splitBuild]_[errorThresholdFactor]_[partitionScheme].iobj
    public static int splitPrune = 4;
    //number of split of query trajectory
    // !shared with query
    public static String distanceType = "FRECHET";
    //available values: FRECHET, DTW, EDR, LCSS
    // !shared with query
    public static double tau = 0.5;
    //similarity threshold for any distance type
    // real value for FRECHET and DTW, integer for EDR and LCSS
    // !shared with query
    public static double eps = 0.003;
    //point-to-point pair distance constraint for EDR & LCSS
    // !shared with query
    public static int spaceConstraint = 3;
    //space/ordering constraint for LCSS
    // !shared with query
    public static String logFilePrefixPrune = "log/prune/LOG_FILE_PRUNE_";
    //set the log file name prefix
    //final log file name: LOG_FILE_PRUNE_[indextype]_[datasetName]_[splitBuild]_
    // [errorThresholdFactor]_[partitionScheme]_[date (yyyyMMddhhmmss)].log
    public static int kKnn = 2;


    //parameters for query processing
    public static String prunedPath = "pruned/";
    public static String prunedFile =
            "pruned_SUBTRAJFLOOD_atc-1000_4_LOG 2_FIX 3_4_FRECHET 0.5";
    public static String logFilePrefixQuery = "log/query/LOG_FILE_QUERY_";
    //set the log file name prefix
    //final log file name: LOG_FILE_QUERY_[indextype]_[datasetName]_[]_[date (yyyyMMddhhmmss)].log

    //parameters for evaluation
    public static String groundTruthPath = "groundTruth/";
    public static String groundTruthFile = "groundTruth porto lcss 0.003 3 5.0.csv";







}
