package Exp;

import AllPointFlood.AllPointIndex;
import AllPointFlood.AllPointIndexBuilder;
import Flood.FloodGroupBuilder;
import Flood.FloodIndicesGroup;
import XFist.Xfist;
import XFist.XfistBuilder;
import Utils.Logger;
import Utils.TrajUtil;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ExperimentBuild {

    private static String logFile;
    private static double errorThreshold;
    private static int splitNumber;
    private static int nBins = 0;
    private static double randMean = 0;
    private static double randDev = 0;
    private static int repeat = 5;
    private static String indexFilename;


    public static void prepareParams(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        String current = dateFormat.format(new Date());
        logFile =  ExpConfig.expPathDefault+
                ExpConfig.logFilePrefixBuild + ExpConfig.indexType + "_" +
                ExpConfig.datasetFilename+ "_"+ExpConfig.sampleSize+ "_"+ ExpConfig.splitBuild + "_" +
                ExpConfig.errorThresholdFactor + "_" + ExpConfig.partitionScheme +"_" + current+".log";
        indexFilename = ExpConfig.indexType+"_"+ExpConfig.datasetFilename+ "_"+
                ExpConfig.splitBuild + "_" + ExpConfig.errorThresholdFactor + "_" +
                ExpConfig.partitionScheme+".iobj";

        String headers = "indexType,datasetName,sampleSize,splitBuild,errorThresholdFactor,partitionScheme,size (kb),buildTime (ms)";
        Logger.createHeaderFile(ExpConfig.expPathDefault+
                ExpConfig.logFilePrefixBuild+"HEADER",headers);

        splitNumber = ExpConfig.splitBuild;
        errorThreshold = 10;
        String[] errorFactorStrings = ExpConfig.errorThresholdFactor.split(" ");
        // available values: LOG x; MUL x; CONST x
        switch (errorFactorStrings[0]){
            case "LOG":
                double logBase = Double.parseDouble(errorFactorStrings[1]);
                errorThreshold = Math.log(TrajUtil.trajectoryDataset.size())/Math.log(logBase);
                break;
            case "MUL":
                double factor = Double.parseDouble(errorFactorStrings[1]);
                errorThreshold = factor * TrajUtil.trajectoryDataset.size();
                break;
            case "CONST":
                errorThreshold = Double.parseDouble(errorFactorStrings[1]);
                break;
        }

        //available values: FIX x, RANDOM x y
        String[] partitionStrings = ExpConfig.partitionScheme.split(" ");
        switch (partitionStrings[0]){
            case "FIX":
                nBins = Integer.parseInt(partitionStrings[1]);
                break;
            case "RANDOM":
                nBins = 0;
                randMean = Double.parseDouble(partitionStrings[1]);
                randDev = Double.parseDouble(partitionStrings[2]);
                break;
        }


        repeat = ExpConfig.repeat;
    }

    public static void run(){
        Logger.startLog(logFile);
        System.out.println("Building index: "+indexFilename);

        try {
            for(int i=0;i<repeat;i++) {
                System.out.println("Running build: "+i+"/"+repeat);
                long startBuildTime;
                long buildTime=0;
                switch (ExpConfig.indexType) {
                    case "SUBTRAJFLOOD":
                        FloodGroupBuilder.configureRandomParams(randMean,randDev);
                        startBuildTime = System.currentTimeMillis();
                        Xfist index = XfistBuilder.build(splitNumber, errorThreshold, nBins);

                        buildTime = System.currentTimeMillis() - startBuildTime;
                        WriteObjectToFile(index, ExpConfig.expPathDefault+ExpConfig.indexPath+ indexFilename+ExpConfig.indexFileExt);
                        WriteObjectToFile(index.getIndex(FloodIndicesGroup.MIN), ExpConfig.expPathDefault+ExpConfig.indexPath+ indexFilename+ExpConfig.indexFileExt+"min");
                        WriteObjectToFile(index.getIndex(FloodIndicesGroup.MAX), ExpConfig.expPathDefault+ExpConfig.indexPath+ indexFilename+ExpConfig.indexFileExt+"max");
                        WriteObjectToFile(index.getIndex(FloodIndicesGroup.FIRST), ExpConfig.expPathDefault+ExpConfig.indexPath+ indexFilename+ExpConfig.indexFileExt+"first");
                        WriteObjectToFile(index.getIndex(FloodIndicesGroup.LAST), ExpConfig.expPathDefault+ExpConfig.indexPath+ indexFilename+ExpConfig.indexFileExt+"last");
                        WriteObjectToFile(index.getIndex(FloodIndicesGroup.SUBMIN), ExpConfig.expPathDefault+ExpConfig.indexPath+ indexFilename+ExpConfig.indexFileExt+"smin");
                        WriteObjectToFile(index.getIndex(FloodIndicesGroup.SUBMAX), ExpConfig.expPathDefault+ExpConfig.indexPath+ indexFilename+ExpConfig.indexFileExt+"smax");
                        WriteObjectToFile(index.getIndex(FloodIndicesGroup.SINGLES), ExpConfig.expPathDefault+ExpConfig.indexPath+ indexFilename+ExpConfig.indexFileExt+"sing");
                        WriteObjectToFile(index.getTable(), ExpConfig.expPathDefault+ExpConfig.indexPath+ indexFilename+ExpConfig.indexFileExt+"stTable");
                        WriteObjectToFile(index.getLengthMap(), ExpConfig.expPathDefault+ExpConfig.indexPath+ indexFilename+ExpConfig.indexFileExt+"lmap");
                    break;

                    case "ALLPOINTFLOOD":
                        AllPointIndexBuilder.configureRandomParams(randMean,randDev);
                        AllPointIndexBuilder.setNPartition(nBins);
                        startBuildTime = System.currentTimeMillis();
                        AllPointIndex allPointIndex = AllPointIndexBuilder.build(TrajUtil.trajectoryDataset,errorThreshold);
                        buildTime = System.currentTimeMillis() - startBuildTime;
                        WriteObjectToFile(allPointIndex, ExpConfig.expPathDefault+ExpConfig.indexPath+ indexFilename+ExpConfig.indexFileExt);

                        break;
                }


                File file = new File(ExpConfig.expPathDefault+ExpConfig.indexPath + indexFilename+ExpConfig.indexFileExt);

                if (file.exists()) {

                    // size of a file (in bytes)
                    long bytes = file.length();
                    long kilobytes = (bytes / 1024);

                    //indexType,datasetName,splitBuild,errorThresholdFactor,partitionScheme
                    Logger.concatLine(ExpConfig.indexType+",");
                    Logger.concatLine(ExpConfig.datasetFilename+",");
                    Logger.concatLine(ExpConfig.sampleSize+",");
                    Logger.concatLine(String.valueOf(splitNumber)+",");
                    Logger.concatLine(ExpConfig.errorThresholdFactor+",");
                    Logger.concatLine(ExpConfig.partitionScheme+",");
                    Logger.concatLine(String.format("%d", kilobytes)+",");
                    Logger.concatLine(String.valueOf(buildTime));
                    Logger.writeLine();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        Logger.endLog();
    }

    public static void WriteObjectToFile(Object serObj,String filepath) {

        try {
            FileOutputStream fileOut = new FileOutputStream(filepath);
            ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
            objectOut.writeObject(serObj);
            objectOut.close();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
