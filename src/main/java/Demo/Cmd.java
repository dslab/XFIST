package Demo;

import Exp.ExperimentBuild;
import Utils.Distance;
import Utils.TrajUtil;
import XFist.Xfist;
import XFist.XfistBuilder;
import XFist.XfistSearchUtil;
import gnu.trove.list.TIntList;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class Cmd {

    private static String demoPath = "sample/";

    private static String sampleDatasetChengdu = demoPath+"chengdu-sample.csv";
    private static String sampleDatasetAtc = demoPath+"atc-sample.csv";
    private static String sampleDataset;

    private static String sampleTestChengdu = demoPath+"chengdu-test.csv";
    private static String sampleTestAtc = demoPath+"atc-test.csv";
    private static String sampleTest;

    private static String datasetName;

    private static List<List<double[]>> testSet;
    private static Xfist index;

    private static double tau;

    public static void main(String[] args){

        int iParam = 1;
        if(args[0].equals("chengdu") || args[0].equals("atc")) {
            datasetName = args[0];
            if(args[0].equals("chengdu")){
                sampleDataset = sampleDatasetChengdu;
                sampleTest = sampleTestChengdu;
            }
            else{
                sampleDataset = sampleDatasetAtc;
                sampleTest = sampleTestAtc;
            }
        }
        else{
            datasetName = "custom";
            sampleDataset = args[0];
            sampleTest = args[1];
            iParam = 2;
        }

        switch(args[iParam].toUpperCase(Locale.ROOT)){
            case "DTW":
                Distance.setCurrentDistance(Distance.DTW);
                tau = Double.parseDouble(args[iParam + 1]);
                break;
            case "DF":
                Distance.setCurrentDistance(Distance.FRECHET);
                tau = Double.parseDouble(args[iParam + 1]);
                break;
            case "EDR":
                Distance.setCurrentDistance(Distance.EDR);
                tau = Double.parseDouble(args[iParam + 1]);
                Distance.setParameters(Double.parseDouble(args[iParam+2]), 0);
                break;
            case "LCSS":
                Distance.setCurrentDistance(Distance.EDR);
                tau = Double.parseDouble(args[iParam + 1]);
                Distance.setParameters(Double.parseDouble(args[iParam+2]), Integer.parseInt(args[iParam+3]));
                break;

        }

        initDatasetAndTest();
        buildXfist();

        System.out.println("Running similarity search query test: "+datasetName);
        for(int i=0;i<testSet.size();i++){
            System.out.println("test "+(i+1)+":");
            TIntList result = XfistSearchUtil.searchSim(index, testSet.get(i),tau,true );
            System.out.println(result);
        }

    }

    private static void initDatasetAndTest(){
        testSet = new ArrayList<>();

        System.out.println("Loading dataset");
        TrajUtil.loadTrajDatasetFromFile(sampleTest);
        for(int i=0;i<TrajUtil.trajectoryDataset.size();i++){
            List<double[]> iTraj = TrajUtil.trajectoryDataset.get(i);
            List<double[]> traj = new ArrayList<>();
            for(int j=0;j<iTraj.size();j++){
                double[] iPoint = iTraj.get(j);
                double[] point = new double[iPoint.length];
                for(int k=0;k<iPoint.length;k++)
                    point[k] = iPoint[k];
                traj.add(point);
            }
            testSet.add(traj);
        }
        TrajUtil.loadTrajDatasetFromFile(sampleDataset);
    }

    private static void buildXfist(){
        System.out.println("Building XFIST of sample dataset: "+datasetName);
        long startBuildTime, buildTime;
        startBuildTime = System.currentTimeMillis();
        try {
            index = XfistBuilder.build(6, 20);
        } catch (Exception e) {
            e.printStackTrace();
        }
        buildTime = System.currentTimeMillis() - startBuildTime;
        ExperimentBuild.WriteObjectToFile(index, demoPath+"xfist-"+datasetName+".xfist");
        File file = new File(demoPath+"xfist-"+datasetName+".xfist");

        if (file.exists()) {
            long bytes = file.length();
            long kilobytes = (bytes / 1024);
            System.out.println("XFist index size: "+String.format("%d", kilobytes)+" kb");
            System.out.println("XFist build time: "+buildTime+" ms");
        }
    }
}
