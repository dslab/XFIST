package Search;

import Utils.Distance;
import Utils.TrajUtil;
import Utils.Util;
import gnu.trove.list.TIntList;
import gnu.trove.list.array.TDoubleArrayList;
import gnu.trove.list.array.TIntArrayList;
import gnu.trove.map.hash.TIntDoubleHashMap;
import gnu.trove.procedure.TIntDoubleProcedure;

import java.util.BitSet;
import java.util.List;
import java.util.PriorityQueue;

public class QueryUtil {
    private static double knnTau;
    private static TIntDoubleHashMap knnCurrent;
    private static BitSet isKnnRemoved;
    private static BitSet isExamined;
    private static double knnMaxDistCurrent;
    private static double knnMinDistRemoved;
    private static final StatEntry knnCurrentStat = new StatEntry();


    public static void initKnn(double tauInit){
        knnCurrent = new TIntDoubleHashMap();
        isKnnRemoved = new BitSet();
        isExamined = new BitSet();
        knnMaxDistCurrent = Double.MIN_VALUE;
        knnMinDistRemoved = Double.MAX_VALUE;
        knnTau = tauInit;
        if(Distance.currentDistance == Distance.EDR || Distance.currentDistance==Distance.LCSS) {
            knnTau = 0;
            knnMaxDistCurrent = 0;
        }
    }

    public static  boolean checkIfExamineDistance(int id){
        //already examined do not examine distance
        return !isExamined.get(id);
    }

    public static void updateRemoved(int id, double distance){
        isKnnRemoved.set(id);
        if(distance< knnMinDistRemoved)
            knnMinDistRemoved = distance;
    }

    public static void addToKnnCurrent(int id, double distance, int k){
        isExamined.set(id);
        System.out.println("Adding");
        System.out.println(knnCurrent);
        System.out.println(distance);
        System.out.println(knnMaxDistCurrent);
        System.out.println("---------");
        if(knnCurrent.size()<k){
            knnCurrent.put(id,distance);
            if(distance > knnMaxDistCurrent)
                knnMaxDistCurrent = distance;
            return;
        }
        int rId = id;
        double rDistance = distance;
        if(distance < knnMaxDistCurrent){
            knnCurrent.forEachEntry(knnCurrentStat);
            knnCurrent.remove(knnCurrentStat.firstMaxKey);
            knnCurrent.put(id,distance);

            knnMaxDistCurrent = knnCurrentStat.secondMax;
            if(distance > knnMaxDistCurrent)
                knnMaxDistCurrent = distance;

            rId = knnCurrentStat.firstMaxKey;
            rDistance = knnCurrentStat.firstMax;

            knnCurrentStat.reset();
        }
        updateRemoved(rId,rDistance);
    }


    ///

    public static double getKnnTau(){
        return knnTau;
    }
    private static void updateTau(){
        if(Distance.currentDistance == Distance.EDR || Distance.currentDistance == Distance.LCSS)
            knnTau = knnTau+1;
        else
            knnTau = knnTau * Math.sqrt(2);
    }

    public static void searchKnnStep(List<double[]> trajQuery,
                                     int k, TIntList searchResult,
                                     TDoubleArrayList distanceList){
        System.out.println(k+" "+searchResult.size()+
                " knnTau:"+knnTau+" maxDist:"+knnMaxDistCurrent);
        for(int i = 0; i< searchResult.size(); i++){
            int id = searchResult.get(i);
            if(!checkIfExamineDistance(id))
                continue;

            double distance;
            if(distanceList!=null)
                distance = distanceList.get(i);
            else
                distance = TrajUtil.computeDistance(TrajUtil.trajectoryDataset.get(id),trajQuery);
            if(Distance.currentDistance == Distance.EDR || Distance.currentDistance == Distance.LCSS){
                distance = distance + Util.sigmoid(TrajUtil.computeDTW(
                        TrajUtil.trajectoryDataset.get(id),trajQuery));
            }

            addToKnnCurrent(id,distance,k);
        }
        //System.out.println(knnCurrent);
        updateTau();
    }

    public static void searchKnnStep(List<double[]> trajQuery,
                                     int k, TIntList searchResult){
        searchKnnStep(trajQuery,k,searchResult,null);
    }



    public static boolean isContinueKnn(int k){
        return knnMaxDistCurrent >= knnTau || knnCurrent.size()<k;
    }

    public static TIntDoubleHashMap outputKnnResult(){
        return knnCurrent;
    }


    public static TIntList searchSim(TIntList searchResult, List<double[]> trajQuery, double tau){
        TIntList ans = new TIntArrayList();
        for (int i = 0; i < searchResult.size(); i++) {
            double distance = TrajUtil.computeDistance(
                    TrajUtil.trajectoryDataset.get(searchResult.get(i)), trajQuery);
            if (distance <= tau)
                ans.add(searchResult.get(i));
        }
        return ans;
    }


    static class StatEntry implements TIntDoubleProcedure {
        int firstMaxKey;
        double firstMax = Double.MIN_VALUE;
        double secondMax = Double.MIN_VALUE;
        @Override
        public boolean execute(int i, double v) {
            if(v>firstMax) {
                if (firstMax != Double.MIN_VALUE)
                    secondMax = firstMax;
                firstMax = v;
                firstMaxKey = i;
            }
            return true;
        }

        public void reset(){
            firstMax = Double.MIN_VALUE;
            secondMax = Double.MIN_VALUE;
        }
    }
}
