package XFist;

import Flood.FloodIndex;
import Utils.*;
import gnu.trove.list.TDoubleList;
import gnu.trove.list.TIntList;
import gnu.trove.list.array.TDoubleArrayList;
import gnu.trove.list.array.TIntArrayList;
import gnu.trove.map.hash.TIntObjectHashMap;
import learnedIndex.LinearRegression;

import java.util.*;

public class XfistPruneUtil {

    public static int datasetSize;

    public static final int MIN = 0;
    public static final int MAX = 1;
    public static final int FIRST = 2;
    public static final int LAST = 3;
    public static final int SUBSTART = 4;
    public static final int SUBMIN = 4;
    public static final int SUBMAX = 5;
    public static final int SINGLES = 6;

    protected static final double INF = 999999.999;

    public static void setDatasetSize(int datasetSize1) {
        datasetSize = datasetSize1;
    }
    protected static BitSet initSubResult(BitSet result, SubTrajTable table, boolean firstOnly){
        BitSet subResult = new BitSet(table.getLength());
        int ikey = 0;
        ikey= result.nextSetBit(ikey);
        for(int t = 0; t< table.getLength();t++){
            if(table.getTidOfPtr(t)<ikey)
                continue;
            if(firstOnly) {
                subResult.set(t);
            }
            else {
                while(t < table.getLength() && table.getTidOfPtr(t) == ikey) {
                    subResult.set(t);
                    t++;
                }
                t--;
            }
            ikey = result.nextSetBit(ikey+1);
            if (ikey == -1)
                break;
        }
        return subResult;
    }

    public static TIntList prune(Xfist index, List<double[]> trajQuery, double tau, int split) {
        double[][] queryPoints = new double[SUBSTART+(2*split)][];

        //List<List<double[]>> subTrajs = MoreTrajUtil.heurSplit(trajQuery.subList(1,trajQuery.size()-1),st);
        //MBR min, MBR max, first, last
        double[][] mbr = TrajUtil.findMBR(trajQuery);
        queryPoints[MIN] = mbr[MIN];
        queryPoints[MAX] = mbr[MAX];
        queryPoints[FIRST] = trajQuery.get(0);
        queryPoints[LAST] = trajQuery.get(trajQuery.size()-1);
        int[] indices = MoreTrajUtil.heurSplit(trajQuery,split-1);
        int iSub = 0;
        int[] lengths = new int[SUBSTART+(2*split)];
        for(int s=0;s<indices.length;s++){
            List<double[]> subTraj;
            if(s==0)
                subTraj = trajQuery.subList( 0,  indices[s]);
            else
                subTraj = trajQuery.subList( indices[s - 1],  indices[s]);
            double[][] subMbr = TrajUtil.findMBR(subTraj);
            queryPoints[SUBSTART+iSub] = subMbr[MIN];
            queryPoints[SUBSTART+iSub+1] = subMbr[MAX];
            lengths[SUBSTART + iSub] = subTraj.size();
            iSub+=2;
        }


        BitSet result = null;

        TIntList intListResult;
        switch (Distance.currentDistance){
            case Distance.DTW:
                result = XfistPruneDTW.search(index,queryPoints,tau);
                break;
            case Distance.FRECHET:
                result = XfistPruneFrechet.search(index,queryPoints,tau);
                break;
            case Distance.EDR:
                result = XfistPruneEdrLcss.search(index,trajQuery.size(),queryPoints,Distance.eps,tau,lengths);
                break;
            case Distance.LCSS:
                result = XfistPruneEdrLcss.search(index,trajQuery.size(),queryPoints,Distance.eps,tau,lengths);
                break;
            default:
                break;
        }

        intListResult = new TIntArrayList(datasetSize);
        if(result.cardinality() == 0)
            return intListResult;
        int i=0;

        while(i!=-1){
            int r = result.nextSetBit(i);
            intListResult.add(r);
            i = result.nextSetBit(r+1);
        }

        return intListResult;
    }

    public static BitSet filterByLength(int trajQueryLength, TIntObjectHashMap lengthMap,
                                        double tau) {
        BitSet result = new BitSet(datasetSize);

        int minLength = trajQueryLength - (int) tau;
        if(minLength<0)
            minLength = 0;

        for(int i=minLength;i<=trajQueryLength+(int) tau;i++){
            TIntArrayList trajs  = (TIntArrayList) lengthMap.get(i);
            if(trajs != null)
                result = ResultOpUtil.addAll(result, trajs);
        }
        return result;
    }

    public static BitSet pruneByIndexIntersect(BitSet prevResult, double[] queryPoint, FloodIndex index, double eps, boolean isIndexMin) {
        BitSet result = new BitSet(datasetSize);
        TIntList tIntListResult = new TIntArrayList();
        double[][] ranges = findRangeIntersect(queryPoint,isIndexMin, index.getSortedDim(),eps);
        TIntList iBins = findBinWithinRange(queryPoint.length-1,ranges[MIN],ranges[MAX],index.getNonSortedDimTails());


        for (int i = 0; i < iBins.size(); i++) {

            int ib = iBins.get(i);

            if (index.getLrSetArray()[ib] == null)
                continue;
            int lowPos = index.getFirstPositionAtBin(ib), highPos = index.getLastPositionAtBin(ib)+1;
            if(isIndexMin)
                highPos = findByKeyAtBin(index, ib, queryPoint, eps, false);
            else
                lowPos = findByKeyAtBin(index, ib, queryPoint, eps, true);

            TIntList resultBin = index.getArrayPtrSubList(lowPos, highPos);
            tIntListResult.addAll(resultBin);

            //***resultBin.sort();
        }
        //System.out.println(tIntListResult.size());
        if (prevResult == null) {
            result = ResultOpUtil.addAll(result, tIntListResult);
        } else {
            result = ResultOpUtil.retainAll(prevResult, tIntListResult);
        }
        return result;
    }

    public static BitSet pruneByIndexMbr(BitSet prevResult, double[][] mbr, FloodIndex index, double eps) {
        BitSet result = new BitSet(datasetSize);
        TIntList tIntListResult = new TIntArrayList();
        double[][] ranges = findRangeMbrEps(mbr,index.getSortedDim(),eps);
        TIntList iBins = findBinWithinRange(mbr[0].length-1,ranges[MIN],ranges[MAX],index.getNonSortedDimTails());

        for (int i = 0; i < iBins.size(); i++) {

            int ib = iBins.get(i);

            if (index.getLrSetArray()[ib] == null)
                continue;
            int lowPos = findByKeyAtBin(index, ib, mbr[MIN], eps, true);
            int highPos = findByKeyAtBin(index, ib, mbr[MAX], eps, false);

            TIntList resultBin = index.getArrayPtrSubList(lowPos, highPos);
            tIntListResult.addAll(resultBin);

        }
        if (prevResult == null) {
            result = ResultOpUtil.addAll(result, tIntListResult);
        } else {
            result = ResultOpUtil.retainAll(prevResult, tIntListResult);
        }
        return result;
    }
    public static BitSet pruneByIndexGeneric(BitSet prevResult, double[] queryPoint, FloodIndex index, double eps) {
        BitSet result = new BitSet(datasetSize);
        TIntList tIntListResult = new TIntArrayList();
        double[][] ranges = findRangeEps(queryPoint,index.getSortedDim(),eps);
        TIntList iBins = findBinWithinRange(queryPoint.length-1,ranges[MIN],ranges[MAX],index.getNonSortedDimTails());

        for (int i = 0; i < iBins.size(); i++) {

            int ib = iBins.get(i);

            if (index.getLrSetArray()[ib] == null)
                continue;
            int lowPos = findByKeyAtBin(index, ib, queryPoint, eps, true);
            int highPos = findByKeyAtBin(index, ib, queryPoint, eps, false);

            TIntList resultBin = index.getArrayPtrSubList(lowPos, highPos);
            tIntListResult.addAll(resultBin);

        }
        if (prevResult == null) {
            result = ResultOpUtil.addAll(result, tIntListResult);
        } else {
            result = ResultOpUtil.retainAll(prevResult, tIntListResult);
        }
        return result;
    }

    protected static double[][] findRangeMbrEps(double[][] mbr, int sortDim, double eps) {

        int dim = mbr[0].length;
        double[] minRange = new double[dim - 1];
        double[] maxRange = new double[dim - 1];

        for (int d = 0; d < dim - 1; d++) {
            int d1 = d;
            if (d >= sortDim) //skips to binsearch on sorted dimension
                d1 = d1 + 1;
            minRange[d] = mbr[MIN][d1] - eps;
            maxRange[d] = mbr[MAX][d1] + eps;
        }

        return new double[][] {minRange, maxRange};
    }
    protected static double[][] findRangeIntersect(double[] point, boolean isIndexMbrMin, int sortDim, double eps) {
        //USED FOR LCSS AND EDR
        //only acquire bins that is in the search +/- eps of the query point area of opposite index
        //Example: (X: min MBR of traj Q, V: max MBR of traj Q)
        // a: indexed MIN MBRs, b: indexed MAX MBRs
        //     0
        //     0      b          a         b        b
        //11111=============================  b
        //  b  |               a           |
        //    a|      b  a             V   |   a
        //    b|              b            |   b
        //  a  |   X      b                |
        //    b|   a     a                 |   a
        //  b  ==============================0000000000000
        // a    b          b             a 1     b
        //                                 1
        //  All b in boundary of 000s to +inf should be queried (isIndexMbrMin: True)
        //  All a in boundary of -inf to 111s should be queried (isIndexMbrMin: False)
        double[] minRange = new double[point.length - 1];
        double[] maxRange = new double[point.length - 1];

        for (int d = 0; d < point.length - 1; d++) {
            int d1 = d;
            if (d >= sortDim) //skips to binsearch on sorted dimension
                d1 = d1 + 1;
            if (isIndexMbrMin) { //check if the current queried is the min of MBR, should take bin from -inf to maxpoint+eps
                minRange[d] = -INF;
                maxRange[d] = point[d1] + eps;
            } else {
                minRange[d] = point[d1] - eps;
                maxRange[d] = INF;
            }
        }

        return new double[][] {minRange, maxRange};
    }


    //for FRECHET and DTW we use 1 point only
    protected static double[][] findRangeEps(double[] point, int sortDim, double eps) {
        //USED FOR Querying bin within bounds
        //only acquire bins (111) that is in each dimension: point-eps 1111 point +eps
        //Example: (X: min MBR of traj Q, V: max MBR of traj Q)
        // c: captured bins
        //     =============================
        //    |     ccccccc                |
        //    |     cccPccc                |
        //    |     ccccccc                |
        //    |                            |
        //    |                            |
        //    ==============================

        double[] minRange = new double[point.length - 1];
        double[] maxRange = new double[point.length - 1];
        for (int d = 0; d < point.length - 1; d++) {
            int d1 = d;
            if (d >= sortDim) //skips to binsearch on sorted dimension
                d1 = d1 + 1;
            minRange[d] = point[d1] - eps;
            maxRange[d] = point[d1] + eps;
        }

        return new double[][] {minRange, maxRange};
    }

    protected static TIntList findBinWithinRange(int dim, double[] minRange, double[] maxRange, TDoubleArrayList[] binTails){
        TIntArrayList prev = null, iBins = null;
        for (int j = 0; j < dim; j++) {
            iBins = new TIntArrayList();
            boolean isInput = false;

            for (int k = 0; k < binTails[j].size(); k++) {
                if (!isInput && minRange[j] <= binTails[j].get(k))
                    isInput = true;

                if (k > 0)
                    if (isInput && maxRange[j] < binTails[j].get(k - 1)) {
                        break;
                    }
                if (isInput) {
                    if (prev == null)
                        iBins.add(k);
                    else
                        for (int i = 0; i < prev.size(); i++)
                            iBins.add(binTails[j].size() * prev.get(i) + k);
                }

            }
            prev = iBins;
        }
        return iBins;
    }

    public static int findByKeyAtBin(FloodIndex index, int ib, //ib: bin number
                                     double[] point, double eps, boolean ifLow) {
        double pos;
        int low, up;

        //head contains the lowest values of keys of the learned index-linear regression at cell ib
        TDoubleArrayList head = index.getLrHeadSet()[ib];
        //findMoreBoundary will find LR head that at index p always .get(p-1) < search value & search value <= .get(p) &
        // lrHeads at cell ib
        //   p-1| p                         | p+1   | p+2
        // .....|{value is always here!}    | ...   | ....
        // exception: p==size-1

        double key = point[index.getSortedDim()] + eps;
        if (ifLow)
            key = point[index.getSortedDim()] - eps;

        int p = Util.findMoreBoundary(head, key);
        LinearRegression lr = index.getLrSetArray()[ib].get(p);
        if (key > lr.tail)
            pos = lr.coef * lr.tail + lr.intercept;
        else
            //search exact index position of point[sortDim]-eps
            pos = lr.coef * key + lr.intercept;

        low = (int) (Math.floor(pos - lr.error));
        if (low < index.getFirstPositionAtBin(ib))
            low = index.getFirstPositionAtBin(ib);
        up = (int) (Math.ceil(pos + lr.error));
        if (up > index.getLastPositionAtBin(ib))
            up = index.getLastPositionAtBin(ib);

        int posInt = (int) pos;
        if ((int)pos < index.getFirstPositionAtBin(ib)) {
            if(up<index.getFirstPositionAtBin(ib))
                return index.getFirstPositionAtBin(ib);
        } else if ((int)pos > index.getLastPositionAtBin(ib)) {
            if(low>index.getLastPositionAtBin(ib))
                return index.getLastPositionAtBin(ib);
            up = index.getLastPositionAtBin(ib);
        }
        else {
            double keyAtPos = index.getKeyAt(posInt);
            if (keyAtPos < key) {
                low = posInt;
            }
            else if (keyAtPos > key) {
                up = posInt;
            }
        }

        if (up > low) {
            TDoubleList subArray = index.getArrayKeySubList(low, up + 1);
            if (ifLow)
                posInt = low + Util.findLessBoundary(subArray, key);

                //findLessBoundary will find LR head that at index p always .get(p) <= search value & search value < .get(p+1) &
                // lrHeads at cell ib
                //   p-1| p                         | p+1   | p+2
                // .....|{value is always here!}    | ...   | ....
                // exception: p==0
            else
                posInt = low + Util.findMoreBoundary(subArray, key);
        } else if (up == low)
            posInt = low;

        if (!ifLow)
            posInt++;
        return posInt;
    }

}