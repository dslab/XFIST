package XFist;

import Flood.FloodIndex;
import Flood.FloodIndicesGroup;
import Utils.TrajUtil;
import gnu.trove.list.array.TIntArrayList;
import gnu.trove.map.hash.TIntObjectHashMap;

import java.io.Serializable;

public class Xfist implements Serializable {
    //default serialVersion id
    private static final long serialVersionUID = 1L;

    private TIntObjectHashMap trajectoryLength;
    private FloodIndicesGroup floodIndicesGroup;
    private SubTrajTable table;
    public final int splitNumber;

    public Xfist(int splitNumber) throws Exception {
        this.splitNumber = splitNumber;
        if(TrajUtil.trajectoryDataset == null){
            throw new Exception("Trajectory data set is not loaded");
        }
        int trSize = TrajUtil.trajectoryDataset.size();
        trajectoryLength =new TIntObjectHashMap();
        for(int i=0;i<trSize;i++) {
            int length = TrajUtil.trajectoryDataset.get(i).size();
            TIntArrayList ids = new TIntArrayList();;
            if(trajectoryLength.containsKey(length))
                ids = (TIntArrayList) trajectoryLength.get(length);

            ids.add(i);
            trajectoryLength.put(length, ids);
        }
    }


    public void init(FloodIndicesGroup floodIndicesGroup, SubTrajTable table){
        this.floodIndicesGroup = floodIndicesGroup;
        this.table = table;
    }

    public FloodIndicesGroup getFloodIndicesGroup(){
        return floodIndicesGroup;
    }

    public FloodIndex getIndex(int indexType){
        return floodIndicesGroup.getIndex(indexType);
    }

    public SubTrajTable getTable(){
        return table;
    }
    public TIntObjectHashMap getLengthMap(){ return trajectoryLength;}
}
