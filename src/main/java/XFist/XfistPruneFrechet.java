package XFist;

import Flood.FloodIndicesGroup;

import java.util.BitSet;

public class XfistPruneFrechet extends XfistPruneUtil {


    public static BitSet search(Xfist index, double[][] queryPoints, double tau){
        FloodIndicesGroup indices = index.getFloodIndicesGroup();
        SubTrajTable table = index.getTable();
        BitSet result = null;
        long start = System.currentTimeMillis();
        result = pruneByIndexGeneric(result,queryPoints[MIN],indices.getIndex(MIN),tau);
        result = pruneByIndexGeneric(result,queryPoints[MAX],indices.getIndex(MAX),tau);

        result = pruneByIndexGeneric(result,queryPoints[FIRST],indices.getIndex(FIRST),tau);
        result = pruneByIndexGeneric(result,queryPoints[LAST],indices.getIndex(LAST),tau);

        BitSet subResultTid = initSubResult(result,table,false);
        BitSet possibleMove = initSubResult(result,table,true);
        BitSet subResultFinal = new BitSet(table.getLength());
        BitSet[] possibleMoveAndSubResult = new BitSet[2];
        for(int i=SUBSTART;i< queryPoints.length;i+=2){
            if(queryPoints[i] == null)
                break;
            BitSet resultMin, resultMax, resultSingle;

            resultMin = pruneByIndexIntersect(subResultTid,queryPoints[i],indices.getIndex(SUBMAX), tau, false );
            resultMax = pruneByIndexIntersect(subResultTid,queryPoints[i+1],indices.getIndex(SUBMIN), tau, true );
            double[][] qMbr = new double[][]{queryPoints[i],queryPoints[i+1]};

            resultSingle = pruneByIndexMbr(subResultTid,qMbr, indices.getIndex(SINGLES), tau);

            resultMin.and(resultMax);
            resultSingle.or(resultMin);
            possibleMoveAndSubResult = computePossibleMoveAndResult(resultSingle,possibleMove,table);
            possibleMove = possibleMoveAndSubResult[1];
        }
        subResultFinal = possibleMoveAndSubResult[0];
        result = new BitSet(datasetSize);
        int iSub = subResultFinal.nextSetBit(0);
        while(iSub!=-1){
            if(table.isLastSub(iSub))
                result.set(table.getTidOfPtr(iSub));
            iSub = subResultFinal.nextSetBit(iSub+1);
        }

        return result;
    }

    public static BitSet[] computePossibleMoveAndResult(BitSet resultI,  BitSet possibleMove,
                                                        SubTrajTable table){

        possibleMove.and(resultI);
        BitSet nextPossibleMove = new BitSet();
        BitSet result = new BitSet();

        int i=possibleMove.nextSetBit(0);
        while(i!=-1){
            result.set(i);
            nextPossibleMove.set(i);
            int j = i+1;
            if(j==table.getLength())
                break;
            while(resultI.get(j) && table.getTidOfPtr(i) == table.getTidOfPtr(j) ) {
                result.set(j);
                nextPossibleMove.set(j);
                j++;
                if(j==table.getLength())
                    break;
            }
            if(j<table.getLength() && table.getTidOfPtr(i) == table.getTidOfPtr(j) )
                nextPossibleMove.set(j);

            i = possibleMove.nextSetBit(j);
        }
        BitSet[] returnValue = {result,nextPossibleMove};
        return returnValue;
    }
}
