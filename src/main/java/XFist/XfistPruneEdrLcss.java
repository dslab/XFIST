package XFist;

import Flood.FloodIndicesGroup;
import Utils.Distance;

import java.util.BitSet;

public class XfistPruneEdrLcss extends XfistPruneUtil {

    public static BitSet search(Xfist index, int trajLength, double[][] queryPoints,
                                double eps, double tau, int[] lengths){

        BitSet result = filterByLength(trajLength,index.getLengthMap(),tau);
        FloodIndicesGroup indices = index.getFloodIndicesGroup();
        SubTrajTable table = index.getTable();

        BitSet resultMin, resultMax;
        long start = System.currentTimeMillis();

        resultMin = pruneByIndexIntersect(result,queryPoints[MIN],indices.getIndex(MAX), eps, false );
        resultMax = pruneByIndexIntersect(result,queryPoints[MAX],indices.getIndex(MIN), eps, true );

        resultMin.and(resultMax);
        result = resultMin;

        BitSet subResultTid = initSubResult(result,table,false);

        int[] distances = null;
        int currentQueryLastIndex = 0, prevQuerySubLength = 0;
        BitSet prevResult = null,resultSingle;
        for(int i=SUBSTART;i< queryPoints.length;i+=2){
            if(queryPoints[i] == null)
                break;

            currentQueryLastIndex = currentQueryLastIndex + lengths[i];
            resultMin = pruneByIndexIntersect(subResultTid,queryPoints[i],indices.getIndex(SUBMAX), eps, false );
            resultMax = pruneByIndexIntersect(subResultTid,queryPoints[i+1],indices.getIndex(SUBMIN), eps, true );
            double[][] qMbr = new double[][]{queryPoints[i],queryPoints[i+1]};
            resultSingle = pruneByIndexMbr(subResultTid,qMbr, indices.getIndex(SINGLES), eps);

            resultMin.and(resultMax);
            resultSingle.or(resultMin);
            if(Distance.currentDistance == Distance.LCSS)
                resultSingle = applySpaceConstraint(resultSingle,table,currentQueryLastIndex,currentQueryLastIndex);

            distances = computeResultEdrLcss(resultSingle,subResultTid,prevResult,distances,table,lengths[i],currentQueryLastIndex,
                    prevQuerySubLength);
            prevResult = resultSingle;
            prevQuerySubLength = currentQueryLastIndex;

//            int iSub = subResultTid.nextSetBit(0);
//            while(iSub!=-1){
//                boolean start = true;
//                int minDist = (int) INF;
//                int iSub2 = iSub;
//                while(!table.isFirstSub(i) || start) {
//                    start = false;
//                    if(minDist>distances[i])
//                        minDist = distances[i];
//                    iSub = subResultTid.nextSetBit(iSub + 1);
//                }
//                if(minDist>tau){
//                    subResultTid.clear(iSub2);
//                    iSub2++;
//                    for(;table.isFirstSub(iSub2);iSub2++)
//                        subResultTid.clear(iSub2);
//
//                }
//            }

        }
        result = new BitSet(datasetSize);

        int iSub = subResultTid.nextSetBit(0);
        while(iSub!=-1){
            if(table.isLastSub(iSub)) {
                if(distances == null || distances[iSub] <= tau)
                    result.set(table.getTidOfPtr(iSub));
            }
            iSub = subResultTid.nextSetBit(iSub+1);
        }

        return result;
    }

    public static BitSet applySpaceConstraint(BitSet subResult, SubTrajTable table,
                                              int curQueryLastIndex, int qLength){
        int minAllow = curQueryLastIndex - qLength- Distance.spaceConstraint;

        if(minAllow<0)
            minAllow = 0;
        int maxAllow = curQueryLastIndex  + Distance.spaceConstraint;

        int i=subResult.nextSetBit(0);
        while(i!=-1){
            int minSubindex = table.getLastIndex(i) - table.getSubLength(i);
            int maxSubindex = table.getLastIndex(i);
            if(!(minAllow<=minSubindex || maxSubindex<=maxAllow))
                subResult.clear(i);
            i=subResult.nextSetBit(i+1);
        }

        return subResult;
    }

    public static int[] computeResultEdrLcss(BitSet resultI, BitSet mbrResults, BitSet prevResult,
                                             int[] curDist, SubTrajTable table, int qLength,
                                             int qLastIndex, int prevQueryLength){
        if(curDist == null){
            curDist = new int[table.getLength()];
            for(int i= mbrResults.nextSetBit(0);i!=-1;i = mbrResults.nextSetBit(i+1)){
                curDist[i] = table.getLastIndex(i);
            }
        }

        /*
         * in DP table illustration....
         * computing distance
         *     existing
         *     ----------------
         *  Q  |PP..|PC..|....|
         *  t   ---------------
         *  r  |CP..|CC..|....|
         *
         * CC is computed from min(PP+subcost,PC+qTrajLength,CP+existingLength)
         * CC: current computation distance
         * PP: previous previous distance (row-1,col-1)
         * CP: current previous distance (row, col-1)
         * PC: previous current distance (row-1, col)
         */

        int PREV_PREV = 0, PREV_CUR = 1, CUR_PREV = 2;

        int prevPrevDist, curPrevDist, prevCurDist, temp;
        boolean start;
        int i = mbrResults.nextSetBit(0);
        while(i!=-1){
            start = true;
            prevPrevDist = qLastIndex - qLength;
            curPrevDist = qLastIndex;
            prevCurDist = curDist[i];

            while(!table.isFirstSub(i) || start){
                int[] costs = computeDistCosts(prevResult,resultI,start,i, table,
                        qLength,prevQueryLength);

                start = false;
                temp = curDist[i];

                int prevCurLength = costs[PREV_CUR];
                int curPrevLength = costs[CUR_PREV];
                int prevPrevLength = costs[PREV_PREV];

                curDist[i] = prevPrevDist + prevPrevLength;
                curDist[i] = Math.min(curDist[i],curPrevDist + curPrevLength);
                curDist[i] = Math.min(curDist[i],prevCurDist + prevCurLength);

                prevPrevDist = temp;
                if(i< table.getLength()-1)
                    prevCurDist = curDist[i+1];
                curPrevDist = curDist[i];
                i = mbrResults.nextSetBit(i+1);
                if(i==-1)
                    break;
            }
        }
        return curDist;
    }


    private static int[] computeDistCosts(BitSet prevResult, BitSet resultI, boolean start, int i,
                                   SubTrajTable table, int currentQueryLength,
                                   int prevQueryLength){
        int prevCurLength = 1;
        int curPrevLength = 1;
        int prevPrevLength = 1;
        if(!start && prevResult != null){
            if(!prevResult.get(i))
                prevCurLength = prevQueryLength;
            if(!resultI.get(i-1))
                curPrevLength = table.getSubLength(i-1);
            if(!prevResult.get(i-1))
                prevPrevLength = Math.min(table.getSubLength(i-1),prevQueryLength);
        }

        if(Distance.currentDistance != Distance.LCSS) {
            if (resultI.get(i)) {
                prevPrevLength = 0;
                if (currentQueryLength > 1)
                    curPrevLength = 0;
                if (table.getSubLength(i)> 1)
                    prevCurLength = 0;
            }
        }
        else {
            if (resultI.get(i)) {
                prevPrevLength = 0;
                if (currentQueryLength > 1)
                    curPrevLength = 0;
                else
                    curPrevLength = (int) INF;
                if (table.getSubLength(i) > 1)
                    prevCurLength = 0;
                else
                    prevCurLength = (int) INF;
            }
            else
                prevPrevLength = (int) INF;
        }

        return new int[]{prevPrevLength, prevCurLength,curPrevLength};
    }

}
