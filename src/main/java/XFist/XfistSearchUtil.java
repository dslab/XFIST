package XFist;

import Utils.TrajUtil;
import gnu.trove.list.TIntList;
import gnu.trove.map.hash.TIntDoubleHashMap;

import java.util.List;

public class XfistSearchUtil {

    public static TIntDoubleHashMap searchKnn(Xfist index,
                                                           List<double[]> trajQuery,
                                                           int k, double tauInit) {
        Search.QueryUtil.initKnn(tauInit);
        while(Search.QueryUtil.isContinueKnn(k)) {
            TIntList result = searchSim(index,trajQuery,Search.QueryUtil.getKnnTau());
            Search.QueryUtil.searchKnnStep(trajQuery,k,result);
        }
        TIntDoubleHashMap knnResult = Search.QueryUtil.outputKnnResult();
        return knnResult;
    }
    public static TIntList searchSim(Xfist index,
                                          List<double[]> trajQuery,
                                          double tau) {
        return searchSim(index,trajQuery,tau,false);
    }
    public static TIntList searchSim(Xfist index,
                                     List<double[]> trajQuery,
                                     double tau, boolean isDemo) {

        long startTime, pruneTime, refineTime;
        startTime = System.nanoTime();
        TIntList searchResult = XfistPruneUtil.prune(index,trajQuery,tau, index.splitNumber);
        pruneTime = (System.nanoTime() - startTime)/1000;
        startTime = System.nanoTime();
        TIntList result = Search.QueryUtil.searchSim(searchResult,trajQuery,tau);
        refineTime = (System.nanoTime() - startTime)/1000;
        if(isDemo) {
            System.out.print("Candidate set size: " + searchResult.size() + "/" + TrajUtil.trajectoryDataset.size()+", ");
            System.out.print("pruning time: "+pruneTime+" mics, ");
            System.out.print("refine time: "+refineTime+" mics, ");
            System.out.println("final set size: "+result.size());
        }
        return result;
    }
}
