package XFist;

import Flood.FloodIndex;
import Flood.FloodIndicesGroup;
import Utils.ResultOpUtil;
import Utils.Util;
import gnu.trove.list.TIntList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.List;

public class XfistPruneDTW extends XfistPruneUtil {

    protected static List<Util.IntDblPair> initSubResult(List<Util.IntDblPair> result, SubTrajTable table){
        List<Util.IntDblPair> subResult = new ArrayList<>();
        int iSub = 0;
        for(int t = 0; t< table.getLength();t++){
            if(table.getTidOfPtr(t)<result.get(iSub).intVal)
                continue;

            subResult.add(new Util.IntDblPair(t,result.get(iSub).dblVal));
            iSub = iSub+1;
            if (iSub == result.size())
                break;
        }
        return subResult;
    }
    public static BitSet search(Xfist index, double[][] queryPoints, double tau){
        FloodIndicesGroup indices = index.getFloodIndicesGroup();
        SubTrajTable table = index.getTable();
        BitSet result = null;
        result = pruneByIndexGeneric(result,queryPoints[MIN],indices.getIndex(MIN),tau);
        result = pruneByIndexGeneric(result,queryPoints[MAX],indices.getIndex(MAX),tau);
        List<Util.IntDblPair> resultDist = null;
        resultDist = searchByFirstLastDtw(result,null,queryPoints[FIRST],indices.getIndex(FIRST),tau);
        resultDist = searchByFirstLastDtw(null,resultDist,queryPoints[LAST],indices.getIndex(LAST),tau);

        result = new BitSet();
        for(int i=0;i<resultDist.size();i++){
            result.set(resultDist.get(i).intVal);
        }

        BitSet subResultTid = initSubResult(result,table,false);
        List<Util.IntDblPair> curDist = initSubResult(resultDist,table);
        List<Util.IntDblPair> intersectDistDtw;
        for(int i=SUBSTART;i< queryPoints.length;i+=2){

            if(queryPoints[i] == null)
                break;
            List<Util.IntDblPair>  resultMin, resultMax, resultSingle;

            resultMin = searchByIndexIntersectDTW(subResultTid,queryPoints[i],indices.getIndex(SUBMAX), tau, false );
            resultMax = searchByIndexIntersectDTW(subResultTid,queryPoints[i+1],indices.getIndex(SUBMIN), tau, true );
            double[][] qMbr = new double[][]{queryPoints[i],queryPoints[i+1]};

            resultSingle = searchByIndexMbrDTW(subResultTid,qMbr, indices.getIndex(SINGLES), tau);

            intersectDistDtw = ResultOpUtil.retainAllDistSortedSorted(resultMin,resultMax,tau,true);
            intersectDistDtw = ResultOpUtil.addAllDistSortedSorted(intersectDistDtw,resultSingle);

            boolean singleFirst = i==SUBSTART && Util.computeDistance(queryPoints[i],queryPoints[i+1])<10e-6;
            boolean singleLast = i==queryPoints.length-2 && Util.computeDistance(queryPoints[i],queryPoints[i+1])<10e-6;
            curDist = computeResultDtw(intersectDistDtw,curDist,table,tau,singleFirst,singleLast);
        }

        result = new BitSet(datasetSize);
        for(int i=0;i<curDist.size();i++){
            if(table.isLastSub(curDist.get(i).intVal))
                result.set(table.getTidOfPtr(curDist.get(i).intVal));
        }
        return result;
    }


    private static List<Util.IntDblPair> searchByFirstLastDtw
            (BitSet prevBitSetResult, List<Util.IntDblPair> prevResultPair,
             double[] queryPoint, FloodIndex index, double eps) {
        List<Util.IntDblPair> currentResultPair = new ArrayList<>();

        if(prevBitSetResult == null){
            prevBitSetResult = new BitSet();
            for(int i=0;i<prevResultPair.size();i++)
                prevBitSetResult.set(prevResultPair.get(i).intVal);
        }

        double[][] ranges = findRangeEps(queryPoint,index.getSortedDim(),eps);
        TIntList iBins = findBinWithinRange(queryPoint.length-1,ranges[MIN],ranges[MAX],index.getNonSortedDimTails());
        for (int i = 0; i < iBins.size(); i++) {
            List<Util.IntDblPair> currentResultPairBin = new ArrayList<>();

            int ib = iBins.get(i);

            if (index.getLrSetArray()[ib] == null)
                continue;
            int lowPos = findByKeyAtBin(index, ib, queryPoint, eps, true);
            int highPos = findByKeyAtBin(index, ib, queryPoint, eps, false);

            TIntList resultBin = index.getArrayPtrSubList(lowPos, highPos);

            for (int j = lowPos; j < highPos; j++) {
                int tid = resultBin.get(j - lowPos);
                if(!prevBitSetResult.get(tid))
                    continue;
                double pseudoDist = Math.abs(queryPoint[index.getSortedDim()] - index.getArray().getKeyAt(j));
                currentResultPairBin.add(new Util.IntDblPair(tid, pseudoDist));
            }
            currentResultPair = ResultOpUtil.addAllDistSortedUnsorted(currentResultPair,currentResultPairBin);
        }

        if (prevResultPair  != null)
            currentResultPair = ResultOpUtil.retainAllDistSortedSorted(prevResultPair, currentResultPair, eps, false);


        return currentResultPair;

    }


    private static List<Util.IntDblPair> searchByIndexMbrDTW(BitSet prevBitSetResult, double[][] mbr, FloodIndex index, double eps) {
        List<Util.IntDblPair> currentResultPair = new ArrayList<>();
        double[][] ranges = findRangeMbrEps(mbr,index.getSortedDim(),eps);
        TIntList iBins = findBinWithinRange(mbr[0].length-1,ranges[MIN],ranges[MAX],index.getNonSortedDimTails());


        for (int i = 0; i < iBins.size(); i++) {
            List<Util.IntDblPair> currentResultPairBin = new ArrayList<>();

            int ib = iBins.get(i);

            if (index.getLrSetArray()[ib] == null)
                continue;
            int lowPos = findByKeyAtBin(index, ib, mbr[MIN], eps, true);
            int highPos = findByKeyAtBin(index, ib, mbr[MAX], eps, false);

            TIntList resultBin = index.getArrayPtrSubList(lowPos, highPos);

            for (int j = lowPos; j < highPos; j++) {
                int tid = resultBin.get(j - lowPos);
                if(!prevBitSetResult.get(tid))
                    continue;
//                double mbrValueMin = mbr[MIN][index.getSortedDim()] ;
//                double mbrValueMax = mbr[MAX][index.getSortedDim()] ;
//                double keyValue =index.getArray().getKeyAt(j);
//                double pseudoDistMin = Math.abs (mbrValueMin - keyValue);
//                double pseudoDistMax = Math.abs (mbrValueMax - keyValue);
//                double pseudoDist = Math.min(pseudoDistMin,pseudoDistMax);
                currentResultPairBin.add(new Util.IntDblPair(tid, 0));
            }
            currentResultPair = ResultOpUtil.addAllDistSortedUnsorted(currentResultPair,currentResultPairBin);
        }
        return currentResultPair;
    }


    private static List<Util.IntDblPair> searchByIndexIntersectDTW(BitSet prevBitSetResult, double[] queryPoint, FloodIndex index, double eps, boolean isIndexMin) {
        List<Util.IntDblPair> currentResultPair = new ArrayList<>();


        double[][] ranges = findRangeIntersect(queryPoint,isIndexMin, index.getSortedDim(),eps);
        TIntList iBins = findBinWithinRange(queryPoint.length-1,ranges[MIN],ranges[MAX],index.getNonSortedDimTails());
        for (int i = 0; i < iBins.size(); i++) {
            List<Util.IntDblPair> currentResultPairBin = new ArrayList<>();

            int ib = iBins.get(i);

            if (index.getLrSetArray()[ib] == null)
                continue;
            int lowPos = index.getFirstPositionAtBin(ib), highPos = index.getLastPositionAtBin(ib)+1;
            if(isIndexMin)
                highPos = findByKeyAtBin(index, ib, queryPoint, eps, false);
            else
                lowPos = findByKeyAtBin(index, ib, queryPoint, eps, true);

            TIntList resultBin = index.getArrayPtrSubList(lowPos, highPos);

            for (int j = lowPos; j < highPos; j++) {
                int tid = resultBin.get(j - lowPos);
                if(!prevBitSetResult.get(tid))
                    continue;
                int mult = 1;
                if(isIndexMin) mult = -1;
                double keyValue =index.getArray().getKeyAt(j);
                double pseudoDist = (mult) *(queryPoint[index.getSortedDim()] - keyValue);
                if(pseudoDist < 0)
                    pseudoDist = 0;

                currentResultPairBin.add(new Util.IntDblPair(tid, pseudoDist));
            }
            currentResultPair = ResultOpUtil.addAllDistSortedUnsorted(currentResultPair,currentResultPairBin);
        }

        return currentResultPair;

    }
    public static List<Util.IntDblPair> computeResultDtw(List<Util.IntDblPair> resultI, List<Util.IntDblPair> prevResult,
                                                         SubTrajTable table, double eps, boolean singleFirst, boolean singleLasts){
        double[] prevDistArray = new double[table.getLength()];
        Arrays.fill(prevDistArray,INF);
        for(int i=0;i<prevResult.size();i++)
            prevDistArray[prevResult.get(i).intVal] = prevResult.get(i).dblVal;

        double[] currentDistArray = new double[table.getLength()];
        Arrays.fill(currentDistArray,INF);
        for(int i=0;i<resultI.size();i++) {
            if(singleFirst && table.isFirstSub(resultI.get(i).intVal))
                currentDistArray[resultI.get(i).intVal] = 0;
            else if(singleLasts && table.isLastSub(resultI.get(i).intVal))
                currentDistArray[resultI.get(i).intVal] = 0;
            else
                currentDistArray[resultI.get(i).intVal] = resultI.get(i).dblVal;
        }

        BitSet firstTids = new BitSet(table.getLength());
        int cTid = -1;
        for(int i=0;i<prevResult.size();i++){
            if( cTid != table.getTidOfPtr(prevResult.get(i).intVal)){
                firstTids.set(prevResult.get(i).intVal);
                cTid = table.getTidOfPtr(prevResult.get(i).intVal);
            }
        }

        List<Util.IntDblPair> currentDistance = new ArrayList<>();

        int p = firstTids.nextSetBit(0);
        while(p!=-1){
            cTid = table.getTidOfPtr(p);
            for(int i=0;p+i < table.getLength() && table.getTidOfPtr(p+i) == cTid ;i++){
                double dist = currentDistArray[p+i];
                double distPrev = prevDistArray[p+i];
                if(i>0){
                    distPrev = Math.min(distPrev,prevDistArray[p+i-1]);
                    distPrev = Math.min(distPrev,currentDistArray[p+i-1]);
                }
                currentDistArray[p+i] = dist + distPrev;
                if(currentDistArray[p+i]<eps)
                    currentDistance.add(new Util.IntDblPair(p+i,currentDistArray[p+i]));
            }
            p = firstTids.nextSetBit(p+1);
        }

        return currentDistance;
    }

}
