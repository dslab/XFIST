package XFist;

import Flood.FloodGroupBuilder;
import Flood.FloodIndicesGroup;
import Utils.SplitAndExtract;
import Utils.TrajUtil;
import gnu.trove.list.array.TIntArrayList;

import java.util.List;

public class XfistBuilder {
    public static Xfist build(int splitNumber, double err, int nBin) throws Exception {
        int dim = TrajUtil.getDim();

        SplitAndExtract.init();
        List<int[]> splitIndices = SplitAndExtract.splitTrajFromDataset(splitNumber);
        SplitAndExtract.extractPointFromDataset(splitIndices);

        SubTrajTable table = new SubTrajTable();

        for(int i=0;i<splitIndices.size();i++){
            for(int j=0;j<splitIndices.get(i).length;j++) {
                table.addEntry(i, splitIndices.get(i)[j]);
            }
        }

        List<List> pointsCollection = SplitAndExtract.getPointCollections();
        List<TIntArrayList> ptrCollection = SplitAndExtract.getPtrCollections();
        FloodGroupBuilder.setNPartition(nBin);
        FloodIndicesGroup floodGroup = FloodGroupBuilder.build(pointsCollection,ptrCollection,dim,err);


        Xfist index = new Xfist(splitNumber);
        index.init(floodGroup,table);
        return index;
    }

    public static Xfist build(int splitNumber, double err) throws Exception {
        return build(splitNumber,err, 0);
    }
}
