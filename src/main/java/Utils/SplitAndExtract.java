package Utils;

import gnu.trove.list.array.TIntArrayList;

import java.util.ArrayList;
import java.util.List;

public class SplitAndExtract {
    private static final int MIN = 0;
    private static final int MAX = 1;
    private static final int FIRST = 2;
    private static final int LAST = 3;
    private static final int SUBMIN = 4;
    private static final int SUBMAX = 5;
    private static final int SINGLES = 6;

    private static List<double[]> mbrMins;
    private static List<double[]> mbrMaxs;
    private static List<double[]> firsts;
    private static List<double[]> lasts;
    private static List<double[]> subMbrMins;
    private static List<double[]> subMbrMaxs;
    private static List<double[]> subSingles;


    private static TIntArrayList ptrNonSubs;
    private static TIntArrayList ptrSubs;
    private static TIntArrayList ptrSubSingles;

    public static void init(){
        mbrMins = new ArrayList<>();
        mbrMaxs = new ArrayList<>();
        firsts = new ArrayList<>();
        lasts = new ArrayList<>();
        subMbrMins = new ArrayList<>();
        subMbrMaxs = new ArrayList<>();
        subSingles = new ArrayList<>();

        ptrNonSubs = new TIntArrayList();
        ptrSubs = new TIntArrayList();
        ptrSubSingles = new TIntArrayList();
    }

    public static List<int[]> splitTrajFromDataset(int splitNumber){
        List<int[]> splitIndices = new ArrayList<>();
        int trSize = TrajUtil.trajectoryDataset.size();
        for(int i=0;i<trSize;i++) {
            List<double[]> traj = TrajUtil.trajectoryDataset.get(i);
            int[] split = MoreTrajUtil.heurSplit(traj, splitNumber - 1);
            splitIndices.add(split);
        }
        return splitIndices;
    }

    public static void extractPointFromDataset(List<int[]> splitIndices){
        int trSize = TrajUtil.trajectoryDataset.size();
        int iSubs = 0;
        for(int i=0;i<trSize;i++){
            List<double[]> traj = TrajUtil.trajectoryDataset.get(i);
            double[][] mbr = TrajUtil.findMBR(traj);
            mbrMins.add(mbr[MIN]);
            mbrMaxs.add(mbr[MAX]);
            firsts.add(traj.get(0));
            lasts.add(traj.get(traj.size()-1));
            ptrNonSubs.add(i);

            List<double[]>[] subTrajs = splitTrajFromIndices(traj,splitIndices.get(i));

            for(int j=0;j<subTrajs.length;j++){
                List<double[]> subTraj = subTrajs[j];
                if(subTraj.size()==1){
                    subSingles.add(subTraj.get(0));
                    ptrSubSingles.add(iSubs++);
                    continue;
                }
                double[][] subMbr = TrajUtil.findMBR(subTraj);
                subMbrMins.add(subMbr[MIN]);
                subMbrMaxs.add(subMbr[MAX]);
                ptrSubs.add(iSubs++);
            }
        }
    }

    public static List<List> getPointCollections(){
        List<List> pointCollection = new ArrayList<>();
        pointCollection.add(mbrMins);
        pointCollection.add(mbrMaxs);
        pointCollection.add(firsts);
        pointCollection.add(lasts);
        pointCollection.add(subMbrMins);
        pointCollection.add(subMbrMaxs);
        pointCollection.add(subSingles);

        return pointCollection;
    }

    public static List<TIntArrayList> getPtrCollections(){
        List<TIntArrayList> ptrCollection = new ArrayList<>();
        ptrCollection.add(ptrNonSubs);
        ptrCollection.add(ptrSubs);
        ptrCollection.add(ptrSubSingles);
        return ptrCollection;
    }

    public static List<double[]>[] splitSingleTraj(List<double[]>traj, int split){
        return splitTrajFromIndices(traj, MoreTrajUtil.heurSplit(traj,split-1));
    }

    private static List<double[]>[] splitTrajFromIndices(List<double[]> traj, int[] splitIndices){
        List[] subTrajs = new List[splitIndices.length];
        for(int j=0;j<splitIndices.length;j++) {
            List<double[]> subTraj;
            if (j == 0)
                subTraj = traj.subList(0, splitIndices[j]);
            else
                subTraj = traj.subList(splitIndices[j - 1], splitIndices[j]);
            subTrajs[j] = subTraj;
        }
        return subTrajs;
    }


}
