package AllPointFlood;

import Flood.FloodIndex;
import Flood.FloodIndexBuilder;
import gnu.trove.list.array.TIntArrayList;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class AllPointIndexBuilder {

    private static double randMean = 10;
    private static double randDev = 5;
    private static int nPartition = 0;
    private static int[] partitionScheme;
    public static void setNPartition(int fixedBinNum){
        nPartition = fixedBinNum;
    }

    public static void configureRandomParams(double randMean_, double randDev_){
        randMean = randMean_;
        randDev = randDev_;
    }


    public static AllPointIndex build(List<List<double[]>> trajectoryDataset,double errThreshold){

        int dim = trajectoryDataset.get(0).get(0).length;

        if(nPartition == 0){
            partitionScheme = createRandompartitions(dim);
        }
        else
            partitionScheme = createFixedPartitions(dim);

        List<double[]> pointCollection = new ArrayList<>();
        TIntArrayList ptrCollection = new TIntArrayList();
        TIntArrayList tidCollection = new TIntArrayList();
        TIntArrayList indexCollection = new TIntArrayList();
        int k = 0;
        for(int i=0;i<trajectoryDataset.size();i++){
            for(int j=0;j<trajectoryDataset.get(i).size();j++){
                pointCollection.add(trajectoryDataset.get(i).get(j));
                ptrCollection.add(k++);
                tidCollection.add(i);
                indexCollection.add(j);
            }
        }

        Random r = new Random();
        int choice = r.nextInt(dim);
        FloodIndex index = FloodIndexBuilder.buildFloodIndex(pointCollection,
                ptrCollection,errThreshold,choice, partitionScheme);
        AllPointIndex allPointIndex = new AllPointIndex(index,indexCollection,tidCollection);

        return allPointIndex;
    }


    private static int[] createRandompartitions(int dim) {
        int[] partitions = new int[dim - 1];
        Random r = new Random();

        for(int j=0;j<dim-1;j++){
            double value = (r.nextGaussian() * randDev) * randMean;
            if(value<1)
                value = 1;
            partitions[j] = (int) value;
        }

        return partitions;
    }

    private static int[] createFixedPartitions(int dim){
        int[] partitions = new int[dim-1];

        for(int j=0;j<dim-1;j++){
            partitions[j] = nPartition;
        }

        return partitions;
    }
}
