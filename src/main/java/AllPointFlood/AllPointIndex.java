package AllPointFlood;

import Flood.FloodIndex;
import gnu.trove.list.array.TIntArrayList;

import java.io.Serializable;

public class AllPointIndex  implements Serializable {
    //default serialVersion id
    private static final long serialVersionUID = 1L;

    private TIntArrayList pointIndex;
    private TIntArrayList tid;

    public static final int TID = 0;
    public static final int INDEX = 1;

    private FloodIndex floodIndex;
    public AllPointIndex(FloodIndex floodIndex, TIntArrayList pointIndex, TIntArrayList tid){
        this.floodIndex = floodIndex;
        this.pointIndex = pointIndex;
        this.tid = tid;
    }

    public FloodIndex getFloodIndex(){
        return floodIndex;
    }

    public TIntArrayList getTid(){
        return tid;
    }

    public TIntArrayList getPointIndex(){
        return pointIndex;
    }

    public int getIndexAt(int ptr){
        return pointIndex.get(ptr);
    }

    public int getTidAt(int ptr){
        return tid.get(ptr);
    }
    public int getLength(){
        return tid.size();
    }
    public boolean isLast(int ptr){
        if (ptr == tid.size() - 1)
            return true;
        return tid.get(ptr) != tid.get(ptr+1);
    }

    public boolean isFirst(int ptr){
        if(ptr==0)
            return true;
        return tid.get(ptr) != tid.get(ptr-1);
    }
}
