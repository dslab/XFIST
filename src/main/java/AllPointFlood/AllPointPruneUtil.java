package AllPointFlood;

import Utils.Distance;
import gnu.trove.list.TIntList;
import gnu.trove.list.array.TIntArrayList;

import java.util.BitSet;
import java.util.List;

public class AllPointPruneUtil {


    public static TIntList prune(AllPointIndex index, List<double[]> trajQuery, double tau) {


        BitSet result = new BitSet();

        TIntList intListResult = new TIntArrayList();
        switch (Distance.currentDistance){
            case Distance.DTW:
            case Distance.FRECHET:
                result = AllPointPruneFrechetDtw.search(index,trajQuery,tau);
                break;
            case Distance.EDR:
            case Distance.LCSS:
                result = AllPointPruneEdrLcss.search(index,trajQuery,tau);
                break;
            default:
                break;
        }
        if(result.cardinality() == 0)
            return intListResult;
        int i=0;

        while(i!=-1){
            int r = result.nextSetBit(i);
            intListResult.add(r);
            i = result.nextSetBit(r+1);
        }

        return intListResult;
    }
}
