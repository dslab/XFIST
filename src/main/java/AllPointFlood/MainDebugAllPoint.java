package AllPointFlood;

import Flood.FloodIndex;
import XFist.XfistPruneUtil;
import Utils.TrajUtil;
import gnu.trove.list.TIntList;
import gnu.trove.list.array.TIntArrayList;

import java.io.*;

public class MainDebugAllPoint {

    private static TIntList[] groundTruth;

    public static void main(String[] args) {

        //TrajUtil.loadTrajDatasetFromFile("../dataset/chengdu-tiny.csv");
        //int trSize = 1000;


        TrajUtil.loadTrajDatasetFromFile("../dataset/atc-subset-f-short.csv");
        int trSize = TrajUtil.trajectoryDataset.size() ;

        TrajUtil.trajectoryDataset = TrajUtil.trajectoryDataset.subList(0,trSize);

        double errorThreshold = 10;
        AllPointIndex index = null;
        for(int i=0;i<10;i++) {
            try {
                long startBuildTime = System.currentTimeMillis();
                AllPointIndexBuilder.setNPartition(3);
                index = AllPointIndexBuilder.build(TrajUtil.trajectoryDataset, errorThreshold);
                System.out.println("Index building time: " + (System.currentTimeMillis() - startBuildTime) + " ms");

                indexDescribe(index);
                WriteObjectToFile(index, "indexAllPoint.obj");
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("index was not created, aborting experiment");
                return;
            }
        }

        long searchStart;
        int tid;
        TIntArrayList test = new TIntArrayList();
        test.add(0);

        XfistPruneUtil.setDatasetSize(trSize);
        double sumPrune = 0;
        double sumQuery = 0;
//EDR LCSS
        double maxTau = 5;
        double inc = 1;
        double tau = 0;
//// CHengdu DTW & Frechet
//        double maxTau = 0.005;
//        double inc = 0.001;
//        double tau = 0.001;


//// ATC DTW & Frechet
//        double maxTau = 0.5;
//        double inc = 0.1;
//        double tau = 0.1;

        int maxIter = 1;
        double avgPrecision = 0;


//        System.out.println("tau,Prune Time,Q Time, avg. Precision,failed Size");
//        for(;tau<=maxTau;tau+=inc) {
//
//            //Distance.setCurrentDistance(Distance.LCSS);
//            Distance.setCurrentDistance(Distance.EDR);
//
//            //Distance.setParameters(0.003,3); //CHengdu
//            Distance.setParameters(0.2,3); //ATC
//
//
//            //Distance.setCurrentDistance(Distance.FRECHET);
//            //String tauStr = String.format("%.3f",tau);
//            String tauStr = String.format("%.1f",tau);
//            String fileAdd = "";
//            if(Distance.currentDistance == Distance.EDR || Distance.currentDistance == Distance.LCSS) {
//                fileAdd = String.format("%.3f",Distance.eps) + " ";
//                if(Distance.currentDistance ==Distance.LCSS)
//                fileAdd = fileAdd+ Distance.spaceConstraint+" ";
//            }
//            //readGroundTruth("groundtruth/groundTruth chengdu "+Distance.getDistanceStr()+" "+fileAdd+tauStr+".csv",trSize);
//            readGroundTruth("groundtruth/groundTruth atc "+Distance.getDistanceStr()+" "+fileAdd+tauStr+".csv",trSize);
//
//
//            for(int iter = 0; iter < maxIter; iter++) {
//                double pruneTime = 0;
//                double queryTime = 0;
//                avgPrecision = 0;
//                TIntList failed = new TIntArrayList();
//
//
//                for (tid = 0; tid < trSize; tid++) {
//                //    for(int tt = 0;tt<test.size();tt++){tid = test.get(tt);
//
//                    List<double[]> qTraj = TrajUtil.trajectoryDataset.get(tid);
//
//                    searchStart = System.currentTimeMillis();
//                    TIntList res = AllPointSearchUtil.search(index, qTraj, tau);
//                    pruneTime = pruneTime + (System.currentTimeMillis() - searchStart);
//
//
//                    //System.out.print(tid);
//                    if (res.containsAll(groundTruth[tid])) {
//                        //System.out.print(" V " + (groundTruth[tid].size()) + "/" + res.size() + " ");
//                        avgPrecision += ((1.0 * groundTruth[tid].size()) / res.size());
//                    } else {
//                        //System.out.print(" X " + (groundTruth[tid].size()) + "/" + res.size() + " ");
//                        failed.add(tid);
//                    }
//                    //System.out.println(res);
//
//                    TIntList resultFlood = new TIntArrayList();
//
//                    searchStart = System.currentTimeMillis();
//                    for (int i = 0; i < res.size(); i++) {
//                        List<double[]> iTraj = TrajUtil.trajectoryDataset.get(res.get(i));
//                        double dist = TrajUtil.computeDistance(qTraj, iTraj);
//                        if (dist <= tau) {
//                            //System.out.println("("+i+","+dist+")");
//                            resultFlood.add(i);
//                        }
//                    }
//                    queryTime = queryTime + (System.currentTimeMillis() - searchStart);
//                    //System.out.println("Search on Pruned " + (System.currentTimeMillis() - searchStart) + " ms");
//                }
//                System.out.print(tau+",");
//                System.out.print(pruneTime + ",");
//                System.out.print(queryTime + ",");
//                System.out.print( (avgPrecision / trSize)+",");
//                System.out.print(failed.size()+",");
//                System.out.println(failed);
//                sumPrune = sumPrune + pruneTime;
//                sumQuery = sumQuery + queryTime;
//            }
//            //System.out.println(tau+","+sumPrune/maxIter+","+sumQuery/maxIter+","+avgPrecision);
//        }

 }
    public static void WriteObjectToFile(Object serObj,String filepath) {

        try {
            FileOutputStream fileOut = new FileOutputStream(filepath);
            ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
            objectOut.writeObject(serObj);
            objectOut.close();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    public static void indexDescribe(AllPointIndex index){
        int lrSize;

        FloodIndex floodIndex = index.getFloodIndex();
        lrSize = 0;
        for (int i = 0; i < floodIndex.getLrHeadSet().length; i++) {
            if (floodIndex.getLrHeadSet()[i] != null) {
                lrSize = lrSize + floodIndex.getLrHeadSet()[i].size();
            }
        }
        System.out.println("Flood  LR Set size: "+lrSize);
    }

    public static void readGroundTruth(String filename, int trSize) {
        groundTruth = new TIntList[trSize];
        File groundTruthFile = new File(filename);
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(groundTruthFile);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fis));
            String input;
            int p = 0;
            while ((input = bufferedReader.readLine()) != null) {
                groundTruth[p] = new TIntArrayList();
                String[] tokenized = input.split(",");
                for (int i = 0; i < tokenized.length; i++) {
                    if (tokenized[i].length() > 0)
                        groundTruth[p].add(Integer.parseInt(tokenized[i]));
                }
                p++;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
