package AllPointFlood;

import Flood.FloodIndex;
import XFist.XfistPruneUtil;
import Utils.Distance;

import java.util.BitSet;
import java.util.List;

public class AllPointPruneEdrLcss extends XfistPruneUtil {


    public static BitSet search(AllPointIndex index, List<double[]> trajQuery, double tau){
        FloodIndex floodIndex = index.getFloodIndex();


        BitSet resultPoint;
        BitSet checkFirst = new BitSet();
        for(int i=0;i<index.getLength();i++)
            if(index.getIndexAt(i)==0)
                checkFirst.set(i);

        int[] curDist = null;

        for(int i=0;i<trajQuery.size();i++){
            double[] qPoint = trajQuery.get(i);
            double[][] qPointMbr = new double[][]{qPoint,qPoint};
            resultPoint = pruneByIndexMbr(null,qPointMbr,floodIndex,Distance.eps);
            if(Distance.currentDistance == Distance.LCSS)
                resultPoint = applySpaceConstraint(index, resultPoint, i);
            curDist = computeResultEdrLcss(resultPoint,curDist,checkFirst,index,i);

            if(i>tau) {
                int j = checkFirst.nextSetBit(0);
                while (j != -1) {
                    boolean start = true;
                    int minVal = (int) INF;
                    for (int k = j; k < index.getLength() && (!index.isFirst(k) || start); k++) {
                        start = false;
                        if (minVal > curDist[k])
                            minVal = curDist[k];
                    }
                    if (minVal > tau)
                        checkFirst.clear(j);
                    j = checkFirst.nextSetBit(j + 1);
                }
            }
        }
        BitSet result = new BitSet();
        int j = checkFirst.nextSetBit(0);
        while(j!=-1){
            result.set(index.getTidAt(j));
            j = checkFirst.nextSetBit(j+1);
        }
        return result;
    }

    private static BitSet applySpaceConstraint(AllPointIndex index, BitSet resultPoint, int curQueryIndex) {
        int minAllow = curQueryIndex - Distance.spaceConstraint;

        if(minAllow<0)
            minAllow = 0;
        int maxAllow = curQueryIndex + Distance.spaceConstraint;

        int i= resultPoint.nextSetBit(0);
        while(i!=-1){
            int tIndex = index.getIndexAt(i);
            if(!(minAllow<=tIndex || tIndex<=maxAllow))
                resultPoint.clear(i);
            i=resultPoint.nextSetBit(i+1);
        }

        return resultPoint;
    }


    public static int[] computeResultEdrLcss(BitSet resultI, int[] curDist, BitSet checkFirst,
                                             AllPointIndex index, int curQueryIndex){
        if(curDist == null){
            curDist = new int[index.getLength()];
            for(int i=0;i<index.getLength();i++)
                curDist[i] = index.getIndexAt(i)+1;
        }

        /*
         * in DP table illustration....
         * computing distance
         *     existing
         *     ----------------
         *  Q  |PP..|PC..|....|
         *  t   ---------------
         *  r  |CP..|CC..|....|
         *
         * CC is computed from min(PP+subcost,PC+qTrajLength,CP+existingLength)
         * CC: current computation distance
         * PP: previous previous distance (row-1,col-1)
         * CP: current previous distance (row, col-1)
         * PC: previous current distance (row-1, col)
         */
        int prevPrevDist, curPrevDist, prevCurDist, temp;
        boolean start;
        int i = checkFirst.nextSetBit(0);
        curQueryIndex =  curQueryIndex + 1;
        while(i!=-1){
            start = true;
            prevCurDist = curDist[i];
            prevPrevDist = curQueryIndex - 1;
            curPrevDist = curQueryIndex;

            for(; i < index.getLength()  && (!index.isFirst(i) || start ); i++){
                start = false;
                temp = curDist[i];

                if(Distance.currentDistance == Distance.EDR) {
                    int subCost = 1;
                    if(resultI.get(i))
                        subCost = 0;
                    curDist[i] = prevPrevDist + subCost;
                    curDist[i] = Math.min(curDist[i], curPrevDist + 1);
                    curDist[i] = Math.min(curDist[i], prevCurDist + 1);
                }
                else if (Distance.currentDistance == Distance.LCSS){
                    if(resultI.get(i))
                        curDist[i] = prevPrevDist;
                    else{
                        curDist[i] = Math.min(curPrevDist, prevCurDist)+1;
                    }
                }

                prevPrevDist = temp;
                if(i< index.getLength()-1)
                    prevCurDist = curDist[i+1];
                curPrevDist = curDist[i];
            }
            i = checkFirst.nextSetBit(i);
        }
        return curDist;
    }
}
