package AllPointFlood;

import Utils.Util;
import gnu.trove.list.TIntList;
import gnu.trove.map.hash.TIntDoubleHashMap;

import java.util.List;
import java.util.PriorityQueue;

public class AllPointQueryUtil {
    public static TIntDoubleHashMap searchKnn(AllPointIndex index,
                                                  List<double[]> trajQuery,
                                                  int k, double tauInit) {
        Search.QueryUtil.initKnn(tauInit);
        while(Search.QueryUtil.isContinueKnn(k)) {
            TIntList result = searchSim(index,trajQuery,Search.QueryUtil.getKnnTau());
            Search.QueryUtil.searchKnnStep(trajQuery,k,result);
        }
        TIntDoubleHashMap knnResult = Search.QueryUtil.outputKnnResult();
        return knnResult;
    }

    public static TIntList searchSim(AllPointIndex index,
                                          List<double[]> trajQuery,
                                          double tau) {

        TIntList searchResult = AllPointPruneUtil.prune(index,trajQuery,tau);
        return Search.QueryUtil.searchSim(searchResult,trajQuery,tau);
    }
}
