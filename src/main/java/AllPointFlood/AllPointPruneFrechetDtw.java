package AllPointFlood;

import Flood.FloodIndex;
import XFist.XfistPruneUtil;
import gnu.trove.list.array.TIntArrayList;

import java.util.BitSet;
import java.util.List;

public class AllPointPruneFrechetDtw extends XfistPruneUtil {

    public static BitSet search(AllPointIndex index, List<double[]> trajQuery, double tau){
        FloodIndex floodIndex = index.getFloodIndex();

        BitSet result = null;
        BitSet resultPoint;

        for(int i=0;i<trajQuery.size();i++){
            double[] qPoint = trajQuery.get(i);
            double[][] qPointMbr = new double[][]{qPoint,qPoint};
            resultPoint = pruneByIndexMbr(null,qPointMbr,floodIndex,tau);
            for(int j=0;j<floodIndex.getArray().getLength();j++){
                if(resultPoint.get(j)) {
                    int ptr= floodIndex.getArray().getPointerAt(j);
                    int tid = index.getTidAt(ptr);
                    int idx = index.getIndexAt(ptr);
                }
            }
            result = computeResult(resultPoint,result,index.getTid(),index.getPointIndex());
        }

        BitSet resultFinal = new BitSet();
        int iSub = result.nextSetBit(0);
        while(iSub!=-1){
            if(index.isLast(iSub))
                resultFinal.set(index.getTid().get(iSub));
            iSub = result.nextSetBit(iSub+1);
        }

        return resultFinal;
    }

    public static BitSet computeResult(BitSet resultI, BitSet currentState, TIntArrayList tid,
                                       TIntArrayList index){
        boolean start = false;
        BitSet iter = new BitSet();
        if(currentState == null){
            currentState = resultI;
            start = true;
        }
        iter.or(currentState);
        iter.or(resultI);


        int i = iter.nextSetBit(0);
        int iTid = -1;
        int prevTid = -1;
        int iIndex = -1;
        int prevIndex = -1;
        boolean clear = false;

        while(i!=-1){
            iTid = tid.get(i);
            iIndex =index.get(i);
            if (iTid != prevTid && index.get(i) == 0 & start){
                clear = false;
            }
            else if (iTid != prevTid && index.get(i) == 0 & !start){
                clear = !currentState.get(i);
            }
            else if((iTid != prevTid && iIndex != 0) || (iTid == prevTid && iIndex - prevIndex > 1)){
                clear = true;
                iter.clear(i);
            }
            if(iTid == prevTid && clear)
                iter.clear(i);

            prevTid = iTid;
            prevIndex = iIndex;
            i = iter.nextSetBit(i+1);
        }

        return iter;
    }
}
