# X-FIST: Extended Flood Index for Efficient Similarity Search in Trajectory Dataset

X-FIST is an system to perform similarity search on trajectory data efficiently with the help of learned multidimensional index Flood.

## Demo guide

Running demo program with [Command Prompt](https://gitlab.com/dslab/XFIST/-/blob/master/src/main/java/Demo/Cmd.java) 
1. Open cmd or terminal
2. Go to the directory of XFIST
3. Run: `java -jar XFIST.jar DEMO [dataset-choice] [distance] [tau] [distance-parameters]`
  - Example 1: running XFIST demo with chengdu sample dataset using DTW similarity distance with tau 0.003
   ```
   java -jar XFIST.jar DEMO chengdu dtw 0.003
   ```
  - Example 2: running XFIST demo with atc sample dataset using EDR similarity distance with tau 5 and epsilon 0.03
   ```
   java -jar XFIST.jar DEMO atc edr 5 0.03
   ```
  - Example 3: running XFIST demo with custom dataset 'dataset.csv' and test 'test.csv'  using LCSS similarity distance with tau 5, epsilon 0.1, and space constraint 2
   ```
   java -jar XFIST.jar DEMO dataset.csv test.csv lcss 5 0.1 2
   ```
4. Parameters explanation: (without '')
- dataset-choice: one or two strings
  - 1 string: 'chengdu' or 'atc', or 
  - 2 strings (custom dataset): [trajectory-dataset-path] and [trajectory-test-path], the format is provided in directory _sample_ with file xx-sample and xx-test, respectively.
- distance: the trajectory distance choice
  - Options: 
    - 'DTW' for Dynamic Time Warping, 
    -  'DF' for Discrete Frechet, 
    -  'EDR' for Edit Distance on Real Sequence, 
    -  'LCSS' for Longest Common Subsequence distance
- tau: the similarity threshold
   - Recommendation (DTW and DF): 0.001 - 0.005 for chengdu, 0.01-0.05 for atc
   - Recommendation (EDR and LCSS): 0-5
- distance parameters:
   - DTW and DF do not require distance parameter
   - EDR and LCSS: 
     - epsilon: threshold for matching point of EDR and LCSS, recommendation: 0.001-0.005 for chengdu, 0.01-0.05 for atc
     - delta: space constraint for LCSS, recommendation: 1-5
